Simple tower defense, where enemz units try to get to your base and destroy it. If the enemy reaches your base, the player will lose one health point. At zero health, the player loses. <br>
If the player can defend his base against all enemies, he wins.


Enemies differ in speed, intelligence to path find to the goal and health points. Enemies can be customized in conf file.
Multiple enemies can stand on one tile.


The player controls the game with a cursor. The player can build buildings to defend himself using points. Some points are received at the start of the game or can be obtained by killing enemies.


There are multiple building types.
Not all buildings attack. Some are support buildings. They can increase attack rate or heal health of nearby buildings.
Some buildings can be builded on higher terrain and some can not. Build turrets in path of your enemies to block them from reaching their goal.



Player can save/laod game anytime.


Controls: Arrows and enter for confirmation
0-9 for building buildings from menu
Saving game: Backspace
Quit game: Q
Destroying towers: D


Save file structure:
\<Money AMOUNT_OF_MONEY \>
\<HP AMOUNT_OF_HP \>
\<Map HEIGHT WIDTH \>
ASCII MAP LINE
.
.
.
ASCII MAP LINE
\<Towers NUMBER_OF_TURETS \>
YCORD XCORD NAME HP CHARGE DMG_UP ATUP_BUFF_TIME ATTACK_SPEEDUP ASUP_BUFF_TIME
.
.
.
YCORD XCORD NAME HP CHARGE DMG_UP ATUP_BUFF_TIME ATTACK_SPEEDUP ASUP_BUFF_TIME
\<Enemy_List NUMBER_OF_ENEMIES \>
YCORD XCORD NAME HP CHARGE ENERGY SLOW SLOW_DURATION
.
.
.
YCORD XCORD NAME HP CHARGE ENERGY SLOW SLOW_DURATION
\<Enemy_Queue NUMBER_QUEUED_ENEMIES \>
NAME
.
.
.
NAME



Conf file structure
\<Enemy NUMBER_OF_TYPES \>
NAME LOOT TYPE HP DMG ATTACK_SPEED ATTACK_RANGE MOVEMENT_INTELIGENCE MOVEMENT_SPEED
.
.
.
NAME LOOT TYPE HP DMG ATTACK_SPEED ATTACK_RANGE MOVEMENT_INTELIGENCE MOVEMENT_SPEED
\<Tower NUMBER_OF_TYPES \>
NAME COST TYPE HP DMG ATTACK_SPEED ATTACK_RANGE
.
.
.
NAME COST TYPE HP DMG ATTACK_SPEED ATTACK_RANGE