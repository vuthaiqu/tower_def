#pragma once

#include <string>
#include "CMatrix.h"
#include "CTile.h"

/**
 * @brief Type of terrain that surounds map. Turrets cannnot be placed on this tile
 * 
 */
class CBorder : public CTile 
{
  public:
		/**
		 * @brief Construct a new CBorder object
		 */
	  CBorder( void );
		/**
		 * @brief Returns char representation of tile to savefile
		 */
    const char saveChar ( void ) const override;
		/**
		 * @brief returns if object with certain properties can be place on this tile. This tile always returns false
		 */
    bool canBeBuild ( bool canBePlacedOnTerrain ) const override;
};
