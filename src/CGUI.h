#pragma once

#include <vector>
#include <string>
#include <ncurses.h>
#include "CScreen.h"
#include "CMenu.h"

/**
 * @brief Class responsible to draw different screens
 */
class CGUI
{
	public:
		/**
		 * @brief Construct a new CGUI object
		 */
		CGUI();
		/**
		 * @brief refreshes screen
		 */
		bool update ();
	private:
		std::shared_ptr < CScreen > m_screen;
};
