#include "CMove.h"

/**
 * @brief Class responsible for moving. Uses BFS and tries to avoid obstacles. Updates every move
 * 
 */
class CUpdateMoving : public CMove
{
	public:
		/**
		 * @brief Construct a new CUpdateMoving object
		 * 
		 * @param map map to claculate path on
		 */
		CUpdateMoving( std::shared_ptr < const CMap > map );
		/**
		 * @brief Returns next move
		 * 
		 * @param from 
		 * @param towers 
		 * @return std::pair < uint8_t , uint8_t > 
		 */
		std::pair < uint8_t , uint8_t > move ( std::pair < uint8_t , uint8_t > from , std::shared_ptr < CTower > ** towers ) override;
};
