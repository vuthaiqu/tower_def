#pragma once

#include <cstdint>
#include <utility>
#include <string>

#include "CMove.h"
#include "CTower.h"
#include "CNonCollisonMoving.h"
#include "CUpdateMoving.h"


/**
 * @brief class representing enemies
 */
class CEnemy
{
 	public:
 		/**
 	 	 * @brief Construct a new CEnemy object. Mostly used to create new enmey in game
 	 	 * 
 	 	 * @param name name of the enemy
 	 	 * @param money amount of money that will dro after killing it
 	 	 * @param hp health points of enemy
 	 	 * @param dmg damge of enemy
 	 	 * @param as attack speed of enemy
 	 	 * @param range range of enenmy
 	 	 * @param speed amount of moves per minute
 	 	 * @param iq movement iq of enemy
 	 	 * @param map surrounding map
 	 	*/
 		CEnemy ( const std::string & name , uint8_t money , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range , uint8_t speed , uint8_t iq ,
						std::shared_ptr < const CMap > map  );
		/**
		 * @brief Construct a new CEnemy object. Mostly used to recreate enemy from savefile
		 * 
 	 	 * @param name name of the enemy
 	 	 * @param money amount of money that will dro after killing it
 	 	 * @param hp health points of enemy
 	 	 * @param dmg damge of enemy
 	 	 * @param as attack speed of enemy
 	 	 * @param range range of enenmy
		 * @param charge charge of enemy attack
 	 	 * @param speed amount of moves per minute
		 * @param energy energy to move
 	 	 * @param iq movement iq of enemy
		 * @param slow movement impaired effect
		 * @param slowDuration movement impaired effect duration
 	 	 * @param map surrounding map
		 */
		CEnemy ( const std::string & name , uint8_t money , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range ,
							uint8_t charge , uint8_t speed , uint8_t energy , uint8_t iq , uint8_t slow , uint8_t slowDuration , std::shared_ptr < const CMap > map );
		/**
		 * @brief Destroy the CEnemy object
		 * 
		 */
		virtual ~CEnemy( void );
		/**
		 * @brief Get the Money object
		 */
		uint8_t getMoney ( void ) const;
		//virtual std::pair < uint8_t , uint8_t > attackEnemies ( void );

		/**
		 * @brief Returns damage to tower when attacking
		 */
		virtual uint8_t attackTowers ( void ) const;
		/**
		 * @brief Charge next attack to tower
		 * 
		 * @return true if is ready to attack, false otherwise
		 */
		bool charge ( void );
		/**
		 * @brief Enemy tries to move to end
		 * 
		 * @param cord current cords
		 * @param towers towers on map
		 * @return std::pair < uint8_t , uint8_t > cords of next move if enemy is ready to move or same cords if enemy is not 
		 */
		std::pair < uint8_t , uint8_t > tryToMove ( std::pair < uint8_t , uint8_t > cord , std::shared_ptr <CTower> ** towers );
		/**
		 * @brief Enemy takes damge
		 * 
		 * @param dmg amount of damge taken 
		 * @return true if enemy died, false otherwise
		 */
		bool takeDmg ( uint8_t dmg );

		/**
		 * @brief Get the Name object 
		 * 
		 * @return reference to name
		 */
		const std::string & getName ( void ) const;
		/**
		 * @brief Get the Icon object
		 * 
		 * @return string form of icon
		 */
		virtual std::string getIcon ( void ) const;
		/**
		 * @brief Get the Hp object
		 */
		virtual uint8_t getHp ( void ) const;
		/**
		 * @brief Prints private values in string to save
		 * 
		 */
		virtual std::string print ( void ) const;

	protected:
		std::string m_name;
		uint8_t m_hp;

		uint8_t m_money;

		uint8_t m_dmg;
		uint8_t m_attackSpeed;
		uint8_t m_attackRange;
		uint8_t m_charge;

		uint8_t m_speed;
		uint8_t m_energy;
		uint8_t m_movementInteligence;

		uint8_t m_slow;
		uint8_t m_slowDuration;

		std::pair < uint8_t , uint8_t > m_nextMove;

		bool m_moved;
		std::unique_ptr < CMove > m_move;

};