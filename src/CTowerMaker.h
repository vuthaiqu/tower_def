#pragma once

#include <tuple>
#include "CTower.h"
#include "CSupport.h"
#include "CBomb.h"
#include "CMelee.h"
/**
 * @brief Class make tower from parameters
 */
class CTowerMaker
{
	public:
		static CTower * constructTower ( std::pair < std::string, std::tuple<std::string, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t> > );
		static CTower * constructTower ( std::pair < std::string, std::tuple<std::string, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t> > params,
																		std::tuple < uint8_t , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t > modifers  );
		const static std::string TOWER_TYPES [ 4 ];
	private:
		CTowerMaker ( void );
};
