#pragma once

#include <vector>
#include <string>
#include <filesystem>
#include <iostream>
#include "CScreen.h"
#include "CGameScreen.h"

/**
 * @brief Class responsible for drawing screen with saved games
 */
class CSaves : public CScreen
{
	public:
		/**
		 * @brief Construct a new CSaves object
		 * 
		 * @param prevScreen previous screen
		 */
		CSaves ( std::shared_ptr < CScreen > prevScreen );
		/**
		 * @brief draws the screen
		 * 
		 * @param self  
		 * @return game screen if save file is selected or previous screen if backspace is pressed 
		 */
		std::shared_ptr < CScreen > active ( std::shared_ptr < CScreen > self ) override;
	private:
		uint8_t m_selection;
		std::vector < std::string > m_savedGames;
		static const std::string m_savedGamesDir;
		bool m_missingFile;

};
