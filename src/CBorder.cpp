#include "CBorder.h"

CBorder::CBorder( void )
	: CTile ()
{
	m_isTerrian = true;
	m_passeble = false;
	m_tileRepresentation.addRow( " ### " )
											.addRow( " ### " )
											.addRow( " ### " );
}

const char CBorder::saveChar ( void ) const
{
	return '#';
}

bool CBorder::canBeBuild ( bool canBePlacedOnTerrain ) const
{
	return false;
}
