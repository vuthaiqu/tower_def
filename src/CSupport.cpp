#include "CSupport.h"


CSupport::CSupport( const std::string & name , uint8_t cost , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range )
	:	CTower ( name , cost , hp , dmg , as , range )
{

}

CSupport::CSupport( const std::string & name , uint8_t cost , uint8_t maxHp , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range , uint8_t energy ,
					uint8_t atup , uint8_t atupTime , uint8_t asup , uint8_t asupTime )
	: CTower ( name , cost , maxHp , hp , dmg , as , range , energy , atup , atupTime , asup , asupTime )
{
	
}

bool CSupport::hillPlacable ( void ) const
{
	return true;
}

std::string CSupport::drawIcon ( void ) const
{
	return "[+]";
}

CTower * CSupport::clone ( void ) const
{
	return new CSupport ( * this );
}

std::pair < std::pair < uint8_t , uint8_t > , uint8_t > CSupport::attackEnemies ( void ) const
{
	return std::make_pair ( std::make_pair ( 0 , 0 ) , 0 );
}

std::pair < std:: pair < std::tuple < uint8_t , uint8_t , uint8_t > , uint8_t > , uint8_t > CSupport::buffTowers ( void ) const
{
	return std::make_pair ( std::make_pair ( std::make_tuple ( m_dmg , m_dmg , m_dmg ) , 60 ) , m_range );
}

