#include <list>
#include <memory>
#include "CTile.h"


/**
 * @brief empty tile. Anything can be placed on this tile also can be passeble for all enemies
 * 
 */
class CEmptyTile : public CTile
{
  public:
		/**
		 * @brief Construct a new CEmptyTile object
		 */
    CEmptyTile( void );
		/**
		 * @brief Returns savable representation of tile
		*/
    const char saveChar ( void ) const override;
		/**
		 * @brief returns if object with certain properties can be place on this tile. This tile always returns false
		 */
    bool canBeBuild ( bool canBePlacedOnTerrain ) const override;

};
