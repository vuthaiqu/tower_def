#include "CMenu.h"	

CMenu::CMenu( std::shared_ptr < CScreen > prevScreen )
	:	CScreen ( prevScreen ),
		m_selection ( 0 )
{

}

const std::string CMenu::m_options [3] = { "NEW GAME" , "LOAD GAME" , "EXIT" };


std::shared_ptr < CScreen > CMenu::active ( std::shared_ptr < CScreen > self )
{
	int ch = getch();
	erase();
	if ( ch == '\n' )
	{
		if ( m_selection == 0 ) // NEW GAME
			return std::make_shared < CNewGame > ( self );
		if ( m_selection == 1 ) // LOAD GAME
			return std::make_shared < CSaves > ( self );
		if ( m_selection == 2 ) // EXIT
			return nullptr;
	}
	if ( ch == KEY_UP )
		m_selection = ( m_selection + 3 - 1 ) % 3;
	if ( ch == KEY_DOWN )
		m_selection = ( m_selection + 1 ) % 3;
	std::string line;
	for ( uint8_t i = 0 ; i < 3 ; i ++ )
	{
		if ( i == m_selection )
			line = "[" + m_options[i] + "]\n";
		else
			line = " " + m_options[i] + "\n";
		printw ( line.c_str() );
	}
	return self;
}
