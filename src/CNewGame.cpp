#include "CNewGame.h"

const std::string CNewGame::m_newGameDir = "./examples/new_games/";

CNewGame::CNewGame( std::shared_ptr < CScreen > prevScreen )
	: CScreen ( prevScreen ),
		m_selection (0),
		m_missingFile ( false )
{
	if ( std::filesystem::exists( m_newGameDir ) )
	{
		for ( const auto & entry :  std::filesystem::directory_iterator( m_newGameDir ) )
			m_newGames.push_back ( entry.path().filename().string() );
		std::sort ( m_newGames.begin() , m_newGames.end() ); 
	} else
	m_missingFile = true;
}


std::shared_ptr < CScreen > CNewGame::active ( std::shared_ptr < CScreen > self )
{
	int ch = getch();
	erase();
	if ( m_missingFile )
	{
		printw("FILE CORRUPTION\nPRESS ANY KEY TO QUIT");
		if ( ch != -1 ) return nullptr;
		return self;
	}

	if ( ch == '\n' )
	{
		std::string fileToLoad = m_newGameDir + m_newGames [ m_selection ];
		return std::shared_ptr < CScreen > ( new CGameScreen ( nullptr , fileToLoad ) );
	}

	if ( ch == KEY_BACKSPACE )
		return m_prevScreen;

	if ( ch == KEY_UP )
		m_selection = ( m_selection + m_newGames.size() - 1 ) %  m_newGames.size() ; 
	if ( ch == KEY_DOWN )
		m_selection = ( m_selection + 1 ) % m_newGames.size();

	std::string line;
	for ( size_t i = 0 ; i < m_newGames.size() ; i ++ )
	{
		if ( m_selection == i )
			line = "[" + m_newGames[i] + "]\n";
		else
			line = " " + m_newGames[i] + "\n";
		printw ( line.c_str() );
	}
	return self;
}
