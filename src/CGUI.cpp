#include "CGUI.h"

CGUI::CGUI ()
{
	initscr();
  noecho();
  curs_set( 0 );
  keypad( stdscr, TRUE );
	timeout ( 20 );
	m_screen = std::make_unique < CMenu > ( nullptr );
}

bool CGUI::update ()
{
	m_screen = m_screen->active ( m_screen );
	if ( m_screen != nullptr )
	{
		refresh();
		move ( 0 , 0 );
	  curs_set( 0 );
		return true;
	}
	else
	{
		endwin();
		return false;
	}
}

