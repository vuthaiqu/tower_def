#include <fstream>
#include <cassert>
#include "CGame.h"

CGame::CGame ( std::shared_ptr < CMap > map , uint32_t money , uint8_t hp , const std::shared_ptr < CConfLoader > conf , uint8_t updateSpeed )
	: m_map( map ),
		m_money ( money ),
		m_playerHp ( hp ),
		m_conf ( conf ),
		m_cursor ( 0 , 0 ),
		m_ctr ( 0 ),
		m_ctrMax ( 500 / updateSpeed ),
		m_spawnCtr ( 0 ),
		m_positionCounter ( 0 )
{
	buildModels();
	m_towerField = new std::shared_ptr< CTower > *  [ m_map->getHeight() ];
	m_fieldOfEnemies = new std::vector < std::shared_ptr < CEnemy > > * [ m_map->getHeight() ];
	m_vissualMap = new CMatrix * [ m_map->getHeight() ];
	for ( uint8_t i = 0 ; i < m_map->getHeight() ; ++ i )
	{
		m_towerField[i] = new std::shared_ptr< CTower > [ m_map->getWidth() ];
		m_fieldOfEnemies[i] = new std::vector < std::shared_ptr < CEnemy > > [ m_map->getWidth() ];
		m_vissualMap[i] = new CMatrix [ m_map->getWidth() ];
	}

}

CGame::~CGame ( void )
{
	for ( uint8_t i = 0 ; i < m_map->getHeight () ; ++ i )
	{
		delete [] m_towerField[i];
		delete [] m_fieldOfEnemies[i];
		delete [] m_vissualMap[i];
	}
	delete [] m_fieldOfEnemies;
	delete [] m_towerField;
	delete [] m_vissualMap;
}

CGame::operator bool ( void ) const
{
	return m_playerHp != 0 ;
}

void CGame::moveLeft ( void )
{
	if ( m_cursor.second > 0 )
		m_cursor.second -= 1;
}

void CGame::moveRight ( void )
{
	if ( m_cursor.second < m_map->getWidth() - 1 )
		m_cursor.second += 1;
}

void CGame::moveUp ( void )
{
	if ( m_cursor.first > 0 )
		m_cursor.first -= 1;
}

void CGame::moveDown ( void )
{
	if ( m_cursor.first < m_map->getHeight() - 1 )
		m_cursor.first += 1;
}


std::vector < std::string > CGame::getGameDisplay ( void ) const
{
	std::vector < std::string > ret;
	for ( uint8_t i = 0 ; i < m_map->getHeight() ; ++ i )
	{
		for ( uint8_t j = 0 ; j < m_map->getWidth() ; ++ j )
		{
			m_vissualMap[i][j] = m_map->getTile( std::make_pair ( i , j ) )->getMatrix();
			if ( m_towerField[i][j] != nullptr )
				m_vissualMap[i][j].drawTower ( m_towerField[i][j] );
			else if ( m_fieldOfEnemies[i][j].size() != 0 )
				m_vissualMap[i][j].drawTwoRows ( m_fieldOfEnemies[i][j].at(0)->getIcon() , std::to_string ( m_fieldOfEnemies[i][j].at(0)->getHp() ) );
		}
	}
	m_vissualMap[ m_cursor.first ][ m_cursor.second ].addCursor();
	for ( uint8_t i = 0 ; i < m_map->getHeight(); ++ i )
	{
		for ( uint8_t j = 0 ; j < CMatrix::getHeight() ; ++ j )
		{
			ret.emplace_back("");
			for ( uint8_t l = 0 ; l < m_map->getWidth() ; ++ l )
				ret.back().append(  m_vissualMap[ i ][ l ].getRow( j ) );
		}
	}
	ret.emplace_back ( "" );
	ret.emplace_back ( "" );
	ret.emplace_back ( " Money " + std::to_string ( m_money ) );
	ret.emplace_back ( " Life " + std::to_string ( m_playerHp ) );
	ret.emplace_back ( "" );
	std::string bo = getBuildOptions();
	if ( bo.size() == 0 )
		ret.emplace_back ( "NOTHING TO BUILD" );
	else
	ret.push_back ( bo );
	ret.push_back ( std::to_string ( m_ctrctr ) + std::string ( " " ) + std::to_string ( m_ctr ) );

	return ret;
}

void CGame::buildModels()
{
	std::map<std::string, std::tuple<std::string, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t>> data = m_conf->getTowerConf();
	for ( std::pair < std::string, std::tuple<std::string, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t> > entry : data )
	{
		m_towerModels.push_back ( std::unique_ptr < CTower >  ( CTowerMaker::constructTower( entry ) ) );
	}
}

void CGame::buildOnInput ( int ch )
{
	int counter = 0;
	if ( m_fieldOfEnemies[ m_cursor.first ][ m_cursor.second ].size() != 0 )
		return;
	if ( m_towerField[ m_cursor.first ][ m_cursor.second ] != nullptr )
		return;
	for ( const std::unique_ptr < CTower > & tower : m_towerModels )
	{
		if ( m_map->getTile( m_cursor )->canBeBuild ( tower->hillPlacable() ) && m_money >= tower->getCost() )
		{
			if ( counter == ch )
			{
				m_money -= tower->getCost();
				setTowerTo ( std::shared_ptr < CTower > ( tower->clone() ) , m_cursor );
				return;
			} else
				++ counter; 
		}
	}
}

bool CGame::setTowerTo ( std::shared_ptr < CTower > tower , std::pair < uint8_t , uint8_t > pos )
{
	if ( pos.first > m_map->getHeight() || pos.second > m_map->getWidth() )
		return false;
	if ( m_towerField [ pos.first ][ pos.second ] == nullptr )
	{
		if ( m_map->getTile( pos )->canBeBuild ( tower->hillPlacable() ) )
		{
			m_towerField [ pos.first ][ pos.second ] = tower;
			return true;
		}
	}
	return false;
}

std::string CGame::getBuildOptions ( void ) const
{
	std::string retval = "";
	if (	m_towerField [ m_cursor.first ] [ m_cursor.second ] == nullptr &&
				m_fieldOfEnemies[ m_cursor.first ][ m_cursor.second ].size() == 0 )
	{
		uint8_t ctr = 0;
		for ( const std::unique_ptr < CTower > & model : m_towerModels )
		{
			if ( m_map->getTile( m_cursor )->canBeBuild ( model->hillPlacable() ) && m_money >= model->getCost() )
			{
				retval.append ( std::to_string ( ctr ++ ) + ". "  );
				retval.append ( model->getName() + " cost: " );
				retval.append ( std::to_string ( model->getCost() ) + " |" );
			}
		}
	}
	return retval;
}


CGame & CGame::destroyTower ( void )
{
	if ( m_towerField [ m_cursor.first ][ m_cursor.second ] != nullptr )
		m_towerField [ m_cursor.first ][ m_cursor.second ] = nullptr;
	return *this;
}


const CMap & CGame::getMap ( void ) const
{
	return ( * m_map );
}

std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CTower > > > CGame::getTowers ( void ) const
{
	std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CTower > > >  retval;
	for ( uint8_t i = 0 ; i < m_map->getHeight() ; ++ i )
		for ( uint8_t j = 0 ; j < m_map->getWidth() ; ++ j )
			if ( m_towerField[i][j] != nullptr )
				retval.emplace_back ( std::make_pair ( i , j ) , m_towerField[i][j] );
	return retval;
}

bool CGame::setEnemyTo ( std::shared_ptr < CEnemy > enemy , std::pair < uint8_t , uint8_t > pos )
{
	if ( pos.first > m_map->getHeight() || pos.second > m_map->getWidth() )
		return false;
	if ( m_towerField [ pos.first ][ pos.second ] == nullptr && m_map->getTile( pos )->isPasseble() )
	{
		m_fieldOfEnemies[ pos.first ][ pos.second ].push_back ( enemy );
		return true;
	}
	return false;
}

void CGame::enemyPhase ( void )
{
	for ( uint8_t i = 0 ; i < m_map->getHeight() ; ++ i )
	{
		for ( uint8_t j = 0 ; j < m_map->getWidth() ; ++ j )
		{
			for ( auto it = m_fieldOfEnemies[i][j].begin() ; it != m_fieldOfEnemies[i][j].end() ;  )
			{
				if ( ( *it )->getHp() == 0 )
				{
					m_money += ( *it )->getMoney();
					m_fieldOfEnemies[i][j].erase( it );
					continue;
				}
				std::pair < uint8_t , uint8_t > nextMove =  ( * it )->tryToMove( std::make_pair ( i , j ) , m_towerField );
			 	if ( nextMove != std::make_pair ( i , j ) )
			 	{
					if ( m_towerField[ nextMove.first ][ nextMove.second ] == nullptr )
					{
						m_fieldOfEnemies[ nextMove.first ][ nextMove.second ].push_back ( ( *it ) );
						m_fieldOfEnemies[i][j].erase( it );
						continue;
					}
					else
					{
						if ( ( * it )->charge() &&  m_towerField[ nextMove.first ][ nextMove.second ]->takeDamge( ( * it )->attackTowers() ) )
						{
							std::pair<std::pair<uint8_t, uint8_t>, uint8_t> attack = m_towerField[ nextMove.first ] [ nextMove.second ]->attackEnemies();
							if ( attack.second != 0 )
							{
								std::list < std::shared_ptr< CEnemy > > effected = getEnemyInRadius( attack.second , nextMove );
								uint8_t ctr = 0;
								for ( std::shared_ptr < CEnemy > & enemy : effected )
								{
									enemy->takeDmg( attack.first.first );
									if ( ctr == attack.first.second )
										break;
									++ ctr;
								}
							}
							m_towerField[ nextMove.first ][ nextMove.second ] = nullptr;
						}
					}
			 	}
				++ it;
			}
		}
	}
}

void CGame::towerPhase ( void )
{
	for ( uint8_t i = 0 ; i < m_map->getHeight() ; ++ i )
	{
		for ( uint8_t j = 0 ; j < m_map->getWidth() ; ++ j )
		{
			if ( m_towerField[i][j] != nullptr  )
			{
				if ( m_towerField[i][j]->charge() )
				{
					std::pair<std::pair<uint8_t, uint8_t>, uint8_t> attack = m_towerField[ i ][ j ]->attackEnemies();
					std::pair < std:: pair < std::tuple < uint8_t , uint8_t , uint8_t > , uint8_t > , uint8_t > buff = m_towerField[ i ][ j ]->buffTowers();
					if ( attack.second != 0 )
					{
						std::list < std::shared_ptr< CEnemy > > targeted = getEnemyInRadius( attack.second , std::make_pair ( i , j ) );
						uint8_t ctr = 0;
						for ( std::shared_ptr< CEnemy > & enemy : targeted )
						{
							if ( ctr == attack.first.second )
								break;
							enemy->takeDmg( attack.first.first );
							++ ctr;
						}
					}
					if ( buff.second != 0 )
					{
						std::list < std::shared_ptr< CTower > > targeted = getTowerInRadius( buff.second , std::make_pair ( i , j ) );
						for ( std::shared_ptr < CTower > & tower : targeted )
							tower->getBuff ( buff.first );
					}
				}
			}
		}
	}
}


std::list < std::shared_ptr< CEnemy > > CGame::getEnemyInRadius ( uint8_t radius , std::pair < uint8_t , uint8_t > center ) const
{
	std::list < std::shared_ptr< CEnemy > > retval;
	std::pair < uint8_t , uint8_t > start;
	std::pair < uint8_t , uint8_t > end;
	if ( center.first < radius )
		start.first = 0;
	else start.first = center.first - radius;
	if ( center.second < radius )
		start.second = 0;
	else start.second = center.second - radius;
	if ( center.first + radius + 1 > m_map->getHeight() )
		end.first = m_map->getHeight();
	else end.first = center.first + radius + 1;
	if ( center.second + radius + 1 > m_map->getWidth() )
		end.second = m_map->getWidth();
	else end.second += center.second + radius + 1;
	for ( uint8_t i = start.first ; i < end.first ; ++ i )
		for ( uint8_t j = start.second ; j < end.second ; ++ j )
			for ( std::shared_ptr < CEnemy > enemy : m_fieldOfEnemies[i][j] )
				retval.push_back ( enemy );
	return retval;
}

void CGame::tick ( void )
{
	checkEnemyOnGoal();
	if ( m_ctr >= m_ctrMax )
	{
		towerPhase();
		enemyPhase();
		++ m_spawnCtr;
		++ m_positionCounter;
		m_ctr = 0;
		++ m_ctrctr;
	}
	else
		++ m_ctr;
	if ( m_spawnCtr == 5 )
	{
		m_spawnCtr = 0;
		std::vector < std::pair < uint8_t , uint8_t > > spawnPoints = m_map->getSpawnPoints();
		if ( ! m_spawnQueue.empty() )
		{
			setEnemyTo ( m_spawnQueue.front() , m_map->getSpawnPoints().at( m_positionCounter % m_map->getSpawnPoints().size() )  );
			m_spawnQueue.pop();
		}
	}
}

std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CEnemy > > > CGame::getEnemyOnMap ( void ) const
{
	std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CEnemy > > > retval;
	for ( uint8_t i = 0 ; i < m_map->getHeight(); ++i  )
		for ( uint8_t j = 0 ;  j < m_map->getWidth(); ++ j )
			for ( std::shared_ptr< CEnemy > enemy : m_fieldOfEnemies[i][j] )
				retval.push_back ( std::make_pair ( std::make_pair ( i , j ) , enemy ) );
	return retval;
}

std::queue < std::shared_ptr < CEnemy > > CGame::getEnemyQueue ( void ) const
{
	return m_spawnQueue;
}

std::list < std::shared_ptr < CTower > > CGame::getTowerInRadius ( uint8_t radius , std::pair < uint8_t , uint8_t > center ) const
{
	std::list < std::shared_ptr < CTower > > retval;
	std::pair < uint8_t , uint8_t > start;
	std::pair < uint8_t , uint8_t > end;
	if ( center.first < radius )
		start.first = 0;
	else start.first = center.first - radius;
	if ( center.second < radius )
		start.second = 0;
	else start.second = center.second - radius;
	if ( center.first + radius + 1 > m_map->getHeight() )
		end.first = m_map->getHeight();
	else end.first += center.first + radius + 1;
	if ( center.second + radius + 1 > m_map->getWidth() )
		end.second = m_map->getWidth();
	else end.second += center.second + radius + 1 ;
	for ( uint8_t i = start.first ; i < end.first ; ++ i )
		for ( uint8_t j = start.second ; j < end.second ; ++ j )
			if ( m_towerField[i][j] != nullptr )
				retval.push_back ( m_towerField[i][j] );
	return retval;
}

void CGame::setQueue ( const std::queue < std::shared_ptr < CEnemy > > & queue )
{
	m_spawnQueue = queue;
}

void CGame::checkEnemyOnGoal ()
{
	for ( const std::pair < uint8_t , uint8_t > goal : m_map->getGoals() )
	{
		for ( auto i = m_fieldOfEnemies[ goal.first ][ goal.second ].begin(); 
					i != m_fieldOfEnemies[ goal.first ][ goal.second ].end() ; )
		{
			-- m_playerHp;
			m_fieldOfEnemies[ goal.first ][ goal.second ].erase ( i );
	
		}
	}
}

uint32_t CGame::getMoney ( void ) const
{
	return m_money;
}

uint8_t CGame::getPlayerHp ( void ) const
{
	return m_playerHp;
}