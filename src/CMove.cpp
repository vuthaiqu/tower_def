#include <cassert>
#include "CMove.h"

CMove::CMove(  std::shared_ptr < const CMap > map )
	: m_mapHeight ( map->getHeight() ),
		m_mapWidth ( map->getWidth() )
{
	m_map = new std::pair < char , uint32_t > * [ map->getHeight() ];
	for ( uint8_t i = 0 ; i < m_mapHeight ; ++ i )
	{
		m_map[i] = new std::pair < char , uint32_t > [ map->getWidth() ];
		for ( uint8_t j = 0 ; j < m_mapWidth ; ++ j )
		{
			if ( map->getTile( std::make_pair ( i , j ) )->isPasseble() )
				m_map[i][j] = std::make_pair ( ' ' , -1 );
			else
				m_map[i][j] = std::make_pair ( 'T' , -1 );
		}
	}
	std::vector < std::pair < uint8_t , uint8_t > > goals = map->getGoals();
	for ( std::pair < uint8_t , uint8_t > goal : goals )
		m_map[ goal.first ][ goal.second ].first = 'S';
}

CMove::~CMove()
{
	for ( uint8_t i = 0 ; i < m_mapHeight; ++ i )
	{
		delete [] m_map[i];
	}
	delete [] m_map;
}

std::pair < uint8_t , uint8_t > CMove::move ( std::pair < uint8_t , uint8_t > from , std::shared_ptr < CTower > ** towers )
{
	if ( m_map[ from.first ][ from.second ].first == 'S' || ( m_path.size() == 0 && ! createPath ( from ) ) )
		return from;
	std::pair < uint8_t , uint8_t > retval;
	retval = m_path.front();
	m_path.pop_front();
	return retval;
}

bool CMove::createPath ( std::pair < uint8_t , uint8_t > from  )
{
	std::queue < std::pair < uint8_t , uint8_t > > queue;
	queue.push( from );
	for ( uint8_t i = 0 ; i < m_mapHeight ; ++ i )
		for ( uint8_t j = 0 ; j < m_mapWidth ; ++ j )
			m_map [ i ] [ j ].second = -1;
	m_map[ from.first ] [ from.second ].second = 0;
	std::pair < uint8_t , uint8_t > active;
	while ( ! queue.empty() )
	{
		active = queue.front();
		if ( m_map [ active.first ][ active.second ].first == 'S' )
		{
			constructPath ( active , from );
			return true;
		}
		inspectNeigbors ( active , queue );
		queue.pop();
	}
	return false;
}

void CMove::inspectNeigbors ( std::pair < uint8_t , uint8_t > active , std::queue < std::pair < uint8_t , uint8_t > > & queue )
{
		if ( active.first > 0 &&
				 ( m_map[ active.first - 1 ][ active.second ].first == ' ' ||
				 m_map[ active.first - 1 ][ active.second ].first == 'S' ) &&
				 m_map[ active.first ][ active.second ].second + 1 < m_map[ active.first - 1 ][ active.second ].second )
			{
				m_map[ active.first - 1 ][ active.second ].second = m_map[ active.first ][ active.second ].second + 1;
				queue.push( std::make_pair ( active.first - 1 , active.second ) );
			}
		if ( active.first < m_mapHeight - 1 &&
				 ( m_map[ active.first + 1 ][ active.second ].first == ' ' ||
				 m_map[ active.first + 1 ][ active.second ].first == 'S' ) &&
				 m_map[ active.first ][ active.second ].second + 1 < m_map[ active.first + 1 ][ active.second ].second )				
		{
				m_map[ active.first + 1 ][ active.second ].second = m_map[ active.first ][ active.second ].second + 1;
				queue.push( std::make_pair ( active.first + 1 , active.second ) );		
		}
		if ( active.second > 0 &&
				 ( m_map[ active.first ][ active.second - 1 ].first == ' ' ||
				 m_map[ active.first ][ active.second - 1 ].first == 'S' ) &&
				 m_map[ active.first ][ active.second ].second + 1 < m_map[ active.first ][ active.second - 1 ].second )				
		{
				m_map[ active.first ][ active.second - 1 ].second = m_map[ active.first ][ active.second ].second + 1;
				queue.push( std::make_pair ( active.first , active.second - 1 ) );
		}
		if ( active.second < m_mapWidth - 1 &&
				 ( m_map[ active.first ][ active.second + 1 ].first == ' ' ||
				 m_map[ active.first ][ active.second + 1 ].first == 'S' ) &&
				 m_map[ active.first ][ active.second ].second + 1 < m_map[ active.first ][ active.second + 1 ].second )				
		{
				m_map[ active.first ][ active.second + 1 ].second = m_map[ active.first ][ active.second ].second + 1;
				queue.push( std::make_pair ( active.first , active.second + 1 ) );
		}
}


void CMove::constructPath ( std::pair < uint8_t , uint8_t > active , std::pair < uint8_t , uint8_t > start )
{
	while ( active != start )
	{
		m_path.push_back ( active );
		if ( active.first > 0 &&
				 ( m_map[ active.first - 1 ][ active.second ].first == ' ' ||
				 m_map[ active.first - 1 ][ active.second ].first == 'S' ) &&
				 m_map[ active.first ][ active.second ].second > m_map[ active.first - 1 ][ active.second ].second )
		{
			active = std::make_pair ( active.first - 1 , active.second );
			continue;
		}
		if ( active.first < m_mapHeight - 1 &&
				 ( m_map[ active.first + 1 ][ active.second ].first == ' ' ||
				 m_map[ active.first + 1 ][ active.second ].first == 'S' ) &&
				 m_map[ active.first ][ active.second ].second > m_map[ active.first + 1 ][ active.second ].second )				
		{
			active = std::make_pair ( active.first + 1 , active.second );
			continue;			
		}
		if ( active.second > 0 &&
				 ( m_map[ active.first ][ active.second - 1 ].first == ' ' ||
				 m_map[ active.first ][ active.second - 1 ].first == 'S' ) &&
				 m_map[ active.first ][ active.second ].second > m_map[ active.first ][ active.second - 1 ].second )				
		{
			active = std::make_pair ( active.first , active.second - 1 );			
			continue;
		}
		if ( active.second < m_mapWidth - 1 &&
				 ( m_map[ active.first ][ active.second + 1 ].first == ' ' ||
				 m_map[ active.first ][ active.second + 1 ].first == 'S' ) &&
				 m_map[ active.first ][ active.second ].second > m_map[ active.first ][ active.second + 1 ].second )				
		{
			active = std::make_pair ( active.first , active.second + 1 );
			continue;
		}
	}
	m_path.reverse();
}


bool CMove::endReachable ( std::pair < uint8_t , uint8_t > from )
{
	std::queue < std::pair < uint8_t , uint8_t > > queue;
	for ( uint8_t i = 0 ; i < m_mapHeight ; ++ i )
		for ( uint8_t j = 0 ; j < m_mapWidth ; ++ j )
			m_map [ from.first ] [ from.second ].second = -1;		
	m_map[ from.first ] [ from.second ].second = 0;
	queue.push( from );
	std::pair < uint8_t , uint8_t > active;
	while ( ! queue.empty() )
	{
		active = queue.front();
		if ( m_map [ active.first ][ active.second ].first == 'S' )
			return true;
		inspectNeigbors ( active , queue );
		queue.pop();
	}
	return false;
}