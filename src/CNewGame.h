#pragma once

#include <filesystem>
#include <string>
#include <algorithm>
#include "CScreen.h"
#include "CGameScreen.h"


/**
 * @brief Class responsible for drawing screen for new games
 */
class CNewGame : public CScreen 
{
public:
	/**
	 * @brief Construct a new CNewGame object
	 * 
	 * @param prevScreen previous screen in case of escaping this screen
	 */
	CNewGame( std::shared_ptr < CScreen > prevScreen );
	/**
	 * @brief Draws a screen
	 * 
	 * @param prev 
	 * @return next screen if option was made or this if nothing was made
	 */
	std::shared_ptr < CScreen > active ( std::shared_ptr < CScreen > active ) override;
private:
	uint8_t m_selection;
	std::vector < std::string > m_newGames;
	static const std::string m_newGameDir;
	bool m_missingFile;
};
