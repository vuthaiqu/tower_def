#pragma once

#include <cstdint>
#include <vector>
#include <memory>
#include "CTile.h"
#include "CTerainTile.h"
#include "CEmptyTile.h"
#include "CBorder.h"
#include "CSpawnTile.h"
#include "CGoalTile.h"

/**
 * @brief Class for in game terrain map
 */
class CMap
{
public:
	/**
	 * @brief Construct a new CMap object
	 * 
	 * @param width width of map
	 * @param height height of map
	 * @param charMap char map loaded from save file
	 */
  CMap( const uint8_t width, const uint8_t height , const std::vector < std::string > & charMap );
	/**
	 * @brief Destroy the CMap object
	 */
  ~CMap();
	/**
	 * @brief Bool operator return true if map was corectly constructed, false otherwises
	 */
	operator bool ( void ) const;
	/**
	 * @brief Get the Tile object
	 */
	const std::shared_ptr <CTile> getTile ( std::pair < uint8_t , uint8_t > cords ) const;
	/**
	 * @brief Get cordinates of all spawn points
	 */
	std::vector < std::pair < uint8_t , uint8_t > > getSpawnPoints ( void ) const;
	/**
	 * @brief Get cordinates of all goals for enemis
	 */
	std::vector < std::pair < uint8_t , uint8_t > > getGoals ( void ) const;
	/**
	 * @brief Get char savable char map
	 */
	std::vector < std::string > getCharMap ( void ) const;
	/**
	 * @brief Get the width of map
	 */
	uint8_t getWidth ( void ) const;
	/**
	 * @brief Get the height of map
	 */
	uint8_t getHeight ( void ) const;
private:
  std::shared_ptr < CTile > ** m_map;
  uint8_t m_width;
  uint8_t m_height;
	std::vector < std::pair < uint8_t , uint8_t > > m_spawnPoints;
	std::vector < std::pair < uint8_t , uint8_t > > m_goals;
	bool m_mapLoaded;
};


