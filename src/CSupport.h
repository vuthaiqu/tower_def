#pragma once

#include "CTower.h"

class CSupport : public CTower
{
	public:
		/**
		 * @brief Construct a new CSupport object
		 * 
		 * @param name name of the tower
		 * @param cost cost of the tower
		 * @param hp health points of the tower
		 * @param buff buff strength
		 * @param bs speed of tower buffing
		 * @param range range of buff
		 */
		CSupport( const std::string & name , uint8_t cost , uint8_t hp , uint8_t buff , uint8_t bs , uint8_t range );
		/**
		 * @brief Construct a new CSupport object, most
		 * 
		 * @param name name of the tower
		 * @param cost cost of the tower
		 * @param maxHp max healt points
		 * @param hp current health points of the tower
		 * @param buff buff strength
		 * @param bs speed of tower buffing
		 * @param range range of buff
		 * @param energy energy to next buff
		 * @param bsup buff strength up to the tower
		 * @param bsupTime buff strength up to the tower durration time
		 * @param bsp buff speed up 
		 * @param bspTime buff speed up time
		 */
		CSupport( const std::string & name , uint8_t cost , uint8_t maxHp , uint8_t hp , uint8_t buff , uint8_t bs , uint8_t range , uint8_t energy ,
							uint8_t bsup , uint8_t bsupTime , uint8_t bsp , uint8_t bspTime );
		/**
		 * @brief Return if this unit is placable on terrain
		 * @return Always return true 
		 */
		bool hillPlacable ( void ) const override;
		/**
		 * @brief Draws icon
		 */
		std::string drawIcon ( void ) const override;
		/**
		 * @brief Returns new dynamicaly allocated copz of self
		 */
		CTower * clone ( void ) const override;
		/**
		 * @brief Returns attack values. First are paired dmg and number of enemies, second is range
		 */
		std::pair < std::pair < uint8_t , uint8_t > , uint8_t > attackEnemies ( void ) const override;
		/**
		 * @brief Pair contains pair of tuple ( heal , attack up , attack speed up ) and duration of buffs and second is range
		 */
		std::pair < std:: pair < std::tuple < uint8_t , uint8_t , uint8_t > , uint8_t > , uint8_t > buffTowers ( void ) const override;

};

