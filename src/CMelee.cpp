#include "CMelee.h"


CMelee::CMelee( const std::string & name , uint8_t cost , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range )
	:	CTower ( name , cost , hp , dmg , as , 1 )
{

}

CMelee::CMelee( const std::string & name , uint8_t cost , uint8_t maxHp , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range , uint8_t energy ,
				uint8_t atup , uint8_t atupTime , uint8_t asup , uint8_t asupTime )
	: CTower ( name , cost , maxHp , hp , dmg , as , 1 , energy , atup , atupTime , asup , asupTime )
{
	
}

bool CMelee::hillPlacable ( void ) const
{
	return false;
}

std::string CMelee::drawIcon ( void ) const
{
	return "--+";
}

CTower * CMelee::clone ( void ) const
{
	return new CMelee ( *this );
}

