#include "CMap.h"



CMap::CMap( const uint8_t width, const uint8_t height , const std::vector < std::string > & charMap )
	:	m_width ( width ),
		m_height ( height ),
		m_mapLoaded ( true )
{
	m_map = new std::shared_ptr<CTile> * [ m_height ];
	for ( uint8_t i = 0 ; i < m_height ; i ++ )
	{
		m_map[i] = new std::shared_ptr<CTile> [ m_width ];
		for ( uint8_t j = 0 ; j < m_width ; j ++ )
		{
			char curChar = charMap.at( i ).at( j );
			switch ( curChar )
			{
			case '.':
				m_map[i][j] = std::make_shared < CEmptyTile > ();
			break;
			case '#':
				m_map[i][j] = std::make_shared < CBorder > ();
			break;
			case 'S':
				m_map[i][j] = std::make_shared < CSpawnTile > ();
				m_spawnPoints.emplace_back ( std::make_pair ( i , j ) );
			break;
			case 'G':
				m_map[i][j] = std::make_shared < CGoalTile > ();
				m_goals.emplace_back ( std::make_pair ( i , j )  );
			break;
			case 'T':
				m_map[i][j] = std::make_shared < CTerainTile > ();
			break;
			default:
				m_mapLoaded = false;
				return;
			break;
			}
		}
	} 
}



CMap::~CMap( void )
{
	for ( uint8_t i = 0 ; i < m_height ; i ++ )
	{
		delete [] m_map[i];
	}
	delete [] m_map;
}

const std::shared_ptr <CTile> CMap::getTile ( std::pair < uint8_t , uint8_t > cords ) const
{
	return m_map[cords.first][cords.second];
}

std::vector < std::pair < uint8_t , uint8_t > >  CMap::getSpawnPoints ( void ) const
{
	return m_spawnPoints;
}

std::vector < std::pair < uint8_t , uint8_t > > CMap::getGoals ( void ) const
{
	return m_goals;
}

uint8_t CMap::getWidth ( void ) const
{
	return m_width;
}

uint8_t CMap::getHeight ( void ) const
{
	return m_height;
}

CMap::operator bool ( void ) const
{
	return m_mapLoaded;
}

std::vector < std::string > CMap::getCharMap ( void ) const
{
	std::vector < std::string > retval;
	for ( uint8_t i = 0 ; i < m_height ; ++ i )
	{
		retval.emplace_back("");
		for ( uint8_t j = 0 ; j < m_width ; ++ j )
		{
			retval.back() += m_map[i][j]->saveChar();
		}
	}
	return retval;

}

