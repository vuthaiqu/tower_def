#pragma once

#include <ncurses.h>
#include <memory>
#include <vector>

/**
 * @brief Abstract class for screens
 * 
 */
class CScreen
{
	public:
		/**
		 * @brief Construct a new CScreen object
		 * 
		 * @param prevScreen previous screen
		 */
		CScreen( std::shared_ptr < CScreen > prevScreen );
		/**
		 * @brief Destroy the CScreen object
		 */
		virtual ~CScreen();
		/**
		 * @brief Draws the screen
		 */ 
		virtual std::shared_ptr < CScreen > active ( std::shared_ptr < CScreen > self ) = 0;
	protected :
		std::shared_ptr < CScreen > m_prevScreen;
};
