#pragma once

#include <memory>
#include <set>
#include <vector>
#include <list>
#include <map>
#include <string>
#include "CScreen.h"
#include "CTowerMaker.h"
#include "CConfLoader.h"
#include "CSaveLoader.h"
#include "CGame.h"
#include "CSaveLoader.h"
#include "CSavingScreen.h"

/**
 * @brief Screen that is responcieble for drawing game into screen
 * 
 */
class CGameScreen : public CScreen
{
	public:
		/**
		 * @brief Construct a new CGameScreen object
		 * 
		 * @param prev ptr to previous screen
		 * @param file save file of the game
		 */
		CGameScreen ( std::shared_ptr < CScreen > prev , const std::string & file );
		/**
		 * @brief Function draws game into screen
		 * 
		 * @param self pointer to self ( kinda cheating shared pointers so they dont deallocate ) 
		 * @return std::shared_ptr < CScreen > 
		 */
		std::shared_ptr < CScreen > active ( std::shared_ptr < CScreen > self ) override;
	private:
		std::string m_file;
		std::unique_ptr < CGame > m_game;
		bool m_ok;
};
