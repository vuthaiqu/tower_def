#include "CBomb.h"


CBomb::CBomb( const std::string & name , uint8_t cost , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range )
	:	CTower ( name , cost , hp , dmg , as , range )
{

}

CBomb::CBomb( const std::string & name , uint8_t cost , uint8_t maxHp , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range , uint8_t energy ,
						uint8_t atup , uint8_t atupTime , uint8_t asup , uint8_t asupTime )
	: CTower ( name , cost , maxHp , hp , dmg , as , range , energy , atup , atupTime , asup , asupTime )
{
	
}

bool CBomb::hillPlacable ( void ) const
{
	return false;
}

std::string CBomb::drawIcon ( void ) const
{
	return "TNT";	
}

CTower * CBomb::clone ( void ) const
{
	return new CBomb ( * this );
}

std::pair < std::pair < uint8_t , uint8_t > , uint8_t > CBomb::attackEnemies ( void ) const
{
	if ( m_hp == 0 )
	{
		return std::make_pair ( std::make_pair ( m_attackBuff + m_dmg , UINT8_MAX ) , m_range );	
	}
	else return std::make_pair ( std::make_pair ( 0 , 0 ) , 0 );
}

void CBomb::getBuff ( std::pair < std::tuple < uint8_t , uint8_t , uint8_t > , uint8_t > buff )
{
	uint8_t heal, atup, asup;
	std::tie ( heal , atup , asup ) = buff.first;

	if ( atup > m_attackBuff )
	{
		m_attackBuff = atup;
		m_attackBuffDuration = buff.second;
	}
	else if ( atup == m_attackBuff )
		m_attackBuffDuration = buff.second;
}

