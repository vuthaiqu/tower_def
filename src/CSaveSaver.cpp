#include "CSaveSaver.h"

CSaveSaver::CSaveSaver( const std::string & fileName )
	: m_ofs ( fileName )
{

}

bool CSaveSaver::saveGame ( const std::unique_ptr < CGame > & game )
{
	if ( ! m_ofs )
		return false;

	saveMoney ( game->getMoney() );
	saveHp ( game->getPlayerHp() );
	saveMap ( game->getMap() );
	saveTowers ( game->getTowers() );
	saveEnemyOnMap ( game->getEnemyOnMap() );
	saveEnemyQueue ( game->getEnemyQueue() );	
 
	return true;
}


void CSaveSaver::saveMap ( const CMap & map )
{
	std::vector < std::string > charMap = map.getCharMap();
	m_ofs << "<Map " << ( int ) charMap.size() << " " << ( int ) charMap[0].length() << " >" << std::endl;
	for ( std::string line : charMap )
	 m_ofs << line << std::endl;
}



void CSaveSaver::saveEnemyQueue ( std::queue < std::shared_ptr < CEnemy > > enemyQueue )
{
	m_ofs << "<Enemy_Queue " << enemyQueue.size() << " >" << std::endl;
	while ( ! enemyQueue.empty() )
	{
		m_ofs << enemyQueue.front()->getName();
		enemyQueue.pop();
		if ( ! enemyQueue.empty() )
			m_ofs << std::endl;
	}
}
void CSaveSaver::saveEnemyOnMap ( const std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CEnemy > > > & list)
{
	m_ofs << "<Enemy_List " << list.size() << " >" << std::endl;
	for ( std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CEnemy > > enemy : list )
		m_ofs << ( int ) enemy.first.first << " " << ( int ) enemy.first.second << " " << enemy.second->print() << std::endl;
}


void CSaveSaver::saveTowers ( const std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CTower > > > & towers )
{
	m_ofs << "<Towers " << ( int ) towers.size() << " >" << std::endl;
	for ( const std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < const CTower > > & tower : towers )
		m_ofs << ( int ) tower.first.first << " " << ( int ) tower.first.second << " " << tower.second->print() << std::endl;
}

void CSaveSaver::saveMoney ( uint32_t money )
{
	m_ofs << "<Money " << money << " >" << std::endl;
}

void CSaveSaver::saveHp ( uint8_t hp )
{
	m_ofs << "<HP " << ( int ) hp << " >" << std::endl;
}


