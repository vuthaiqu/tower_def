#include "CMatrix.h"


const uint8_t CMatrix::m_height = 3;
const uint8_t CMatrix::m_width = 5;

CMatrix::CMatrix(  )
{
}

CMatrix & CMatrix::addRow ( const std::string & row )
{
	if ( row.size() == m_width && m_matrix.size() < m_height )
	{
		m_matrix.emplace_back ( row );
	}
	return * this;
}

CMatrix & CMatrix::addCursor ( void )
{
	for ( std::string & line : m_matrix )
	{
		line[ 0 ] = '(';
		line[ m_width - 1 ] = ')';
	}
	return *this;
}

uint8_t CMatrix::getHeight ( void )
{
	return m_height;
}

uint8_t CMatrix::getWidth ( void )
{
	return m_width;
}

std::string CMatrix::getRow ( uint8_t row ) const
{
	if ( row < CMatrix::m_height )
	{
		return m_matrix [ row ];
	}
	return "";
}

CMatrix & CMatrix::drawTower ( std::weak_ptr < CTower > tower )
{
	if ( ! tower.expired() )
	{
		for ( uint8_t i = 0 ; i < m_width ; ++ i)
		{
			m_matrix[0][i] = ' ';
			m_matrix[1][i] = ' ';
		}
		std::string tmp = tower.lock()->drawIcon();
		if ( tmp.size() <= 3 )
			for ( uint8_t i = 0 ; i < tmp.size() ; ++ i )
				m_matrix[0][ i + 1 ] = tmp[i];
		tmp = tower.lock()->drawHp();
		if ( tmp.size() <= 3 )
			for ( uint8_t i = 0 ; i < tmp.size() ; ++ i )
				m_matrix[1][ i + 1 ] = tmp[i];
	}
	return * this;
}

CMatrix & CMatrix::drawTwoRows ( std::string first , std::string second  )
{
	for ( uint8_t i = 0 ; i < m_width ; ++ i)
	{
		m_matrix[0][i] = ' ';
		m_matrix[1][i] = ' ';
	}
	if ( first.size() <= 3 )
		for ( uint8_t i = 0 ; i < first.size() ; ++ i )
			m_matrix[0][ i + 1 ] = first[i];
	if ( second.size() <= 3 )
		for ( uint8_t i = 0 ; i < second.size() ; ++ i )
			m_matrix[1][ i + 1 ] = second[i];
	return * this;
}

