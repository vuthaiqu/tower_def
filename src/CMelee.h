#pragma once

#include "CTower.h"
/**
 * @brief Class representing melee uinits, that cannot be placed on hill
 */
class CMelee : public CTower
{
public:
	/**
	 * @brief Construct a new CMelee object. Mostly used to create new unit in game
	 * 
	 * @param name name of the melee unit
	 * @param cost cost to build melee unit
	 * @param hp health points of melee uint
	 * @param dmg dmg of melee unit
	 * @param as attack speed of melee unit
	 * @param range range of melee unit
	 */
	CMelee( const std::string & name , uint8_t cost , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range );
	CMelee( const std::string & name , uint8_t cost , uint8_t maxHp , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range , uint8_t energy ,
							uint8_t atup , uint8_t atupTime , uint8_t asup , uint8_t asupTime );

//		std::string drawStats ( void ) const override;
//		std::string printInfo ( void ) const override;
		/**
		 * @brief Returns if this unit can be place on terrain. This class always returns false
		 */
		bool hillPlacable ( void ) const override;
		/**
		 * @brief Returns string of vissual representation of unit
		 */
		std::string drawIcon ( void ) const override;
		/**
		 * @brief Returns new dynamicaly allocated instance of self copy
		 */
		CTower * clone ( void ) const override;


};
