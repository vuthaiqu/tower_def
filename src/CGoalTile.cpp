#include "CGoalTile.h"

CGoalTile::CGoalTile()
	:	CTile()
{
	m_passeble = true;
	m_tileRepresentation.addRow( " /V\\ " )
										 .addRow( " \\ / " )
											.addRow( "  V  " );
}

const char CGoalTile::saveChar ( void ) const
{
	return 'G';
}

bool CGoalTile::canBeBuild ( bool canBePlacedOnTerrain ) const
{
	return false;
}