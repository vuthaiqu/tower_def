#include "CTile.h"

/**
 * @brief Goal tile for enemies
 */
class CGoalTile : public CTile
{
	public:
		/**
	 	 * @brief Construct a new CGoalTile object
	 	*/
		CGoalTile();
		/**
		 * @brief Returns savable representation of tile
		 */
    const char saveChar ( void ) const override;

		/**
		 * @brief returns if object with certain properties can be place on this tile. This tile always returns false
		 */
    bool canBeBuild ( bool canBePlacedOnTerrain ) const override;
};
