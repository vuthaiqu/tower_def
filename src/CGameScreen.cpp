#include <cassert>
#include "CGameScreen.h"

CGameScreen::CGameScreen ( std::shared_ptr < CScreen > prev , const std::string & file )
	:	CScreen ( prev ),
		m_file ( file ),
		m_ok ( true )
{	
	std::shared_ptr < CConfLoader > conf ( new CConfLoader () );
	if ( ! *conf  )
	{
		m_ok = false;
		return;
	}
	CSaveLoader sl ( m_file , conf );
	if ( ! sl )
	{
		m_ok = false;
		return;
	}
	m_game = sl.loadGame( 20 );
	if ( m_game == nullptr )
	{
		m_ok = false;
		return;
	}
}


std::shared_ptr < CScreen > CGameScreen::active ( std::shared_ptr < CScreen > self )
{
	int ch = getch();
	erase();

	if ( ! m_ok )
	{
		printw ( " FILE ERR\n PRESS ANY KEY TO EXIT " );
		if ( ch != -1 )
			return nullptr;
		return self;
	}

	if ( ! ( * m_game ) )
	{
		printw ( " GAME OVER\n PRESS ANY KEY TO EXIT " );
		if ( ch != -1 )
			return nullptr;
		return self;
	}

	if ( ch == KEY_BACKSPACE )
	{
		return std::make_shared < CSavingScreen > ( self , m_game );
	}

	if ( ch >= '0' || ch <= '9' )
	{
		m_game->buildOnInput( ch - '0' );
	}

	if ( ch == 'd' )
	{
		m_game->destroyTower();
	}

	if ( ch == KEY_RIGHT )
		m_game->moveRight();
	if ( ch == KEY_LEFT )
		m_game->moveLeft();
	if ( ch == KEY_UP )
		m_game->moveUp();
	if ( ch == KEY_DOWN )
		m_game->moveDown();
	m_game->tick();
	std::vector < std::string > screen = m_game->getGameDisplay();
	for ( std::string line : screen )
	{
		line = line + "\n";
		printw ( line.c_str() );
	}

	return self;
}