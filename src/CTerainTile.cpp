#include "CTerainTile.h"

CTerainTile::CTerainTile()
	: CTile ()
{
	m_passeble = false;
	m_isTerrian = true;
	m_tileRepresentation.addRow( " MMM " )
											.addRow( " MMM " )
											.addRow( " MMM " );

}

const char CTerainTile::saveChar ( void ) const
{
	return 'T';
}

bool CTerainTile::canBeBuild ( bool canBePlacedOnTerrain ) const
{
	return canBePlacedOnTerrain;
}
