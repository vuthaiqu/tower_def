#include "CSavingScreen.h"

const std::string CSavingScreen::SAVE_DIRECTORY = "./examples/saved_games";

CSavingScreen::CSavingScreen( std::shared_ptr < CScreen > prevScreen , const std::unique_ptr<CGame> & game )
	: CScreen ( prevScreen ),
		m_game ( game ),
		m_saveName (""),
		m_saved ( false ),
		m_err ( false )
{
}


std::shared_ptr < CScreen > CSavingScreen::active ( std::shared_ptr < CScreen > prev )
{
	int ch = getch();
	erase();
	if ( m_saved )
	{
		printw ( " GAME SEVED PRESS ANY KEY TO ESCAPE\n" );
		if ( ch != -1 )
			return nullptr;
		return prev;
	}

	if ( m_err )
	{
		printw ( " GAME DID NOT SAVE PRESS ANY KEY TO COTINUE GAME\n" );
		if ( ch != -1 )
			return m_prevScreen;
		return prev;
	}

	if ( ch == '\n' )
	{
		CSaveSaver save ( SAVE_DIRECTORY + std::string ("/") +  m_saveName );
		if ( save.saveGame( m_game ) )
		{
			m_saved = true;
			return prev;
		}
		else
		{
			m_err = true;
			return prev;
		}
	}

	
	if ( ( 'a' <= ch && ch >= 'z' ) || ( 'A' <= ch && ch >= 'Z' ) || ( '0' <= ch && ch >= '9' ) )
	{
		m_saveName += ( char ) ch;
	}

	if ( ch == KEY_BACKSPACE )
	{
		return m_prevScreen;
	}
	std::string tmp = " Savefile name: ";
	tmp.append( m_saveName );
	printw ( tmp.c_str() );
	return prev;
}
