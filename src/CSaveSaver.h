#include <string>
#include <fstream>
#include <queue>

#include "CEnemy.h"
#include "CTower.h"
#include "CGame.h"


/**
 * @brief Class responcible for saving game
 */
class CSaveSaver
{
	public:
		/**
		 * @brief Construct a new CSaveSaver object
		 * 
		 * @param fileName name of file to save game into
		 */
		CSaveSaver( const std::string & fileName );
		/**
		 * @brief Saves the game
		 * 
		 * @param game Game to be saved
		 * @return true if saved successfully, false otherwise 
		 */
		bool saveGame ( const std::unique_ptr < CGame > & game );
	private:
		void saveMap ( const CMap & map );
		void saveEnemyQueue ( std::queue < std::shared_ptr < CEnemy > > queue );
		void saveEnemyOnMap ( const std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CEnemy > > > & list );
		void saveTowers ( const std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CTower > > > & towers );
		void saveMoney ( uint32_t money );
		void saveHp ( uint8_t money );
		std::ofstream m_ofs;

};
