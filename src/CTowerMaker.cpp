#include "CTowerMaker.h"

const std::string CTowerMaker::TOWER_TYPES [ 4 ] = { "Tower" , "Melee" , "Bomb" , "Support" };

CTower * CTowerMaker::constructTower ( std::pair < std::string, std::tuple<std::string, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t> > params )
{
		std::string type;
		uint8_t cost, hp, dmg, as, ar;
		std::tie ( type, cost, hp, dmg, as, ar ) = params.second;
		
		if ( type == TOWER_TYPES[0] )
		{
			return new CTower ( params.first , cost , hp , dmg , as , ar );
		}
		else if ( type == TOWER_TYPES[1] )
		{
			return new CMelee ( params.first , cost , hp , dmg , as , 1 );	
		}
		else if ( type == TOWER_TYPES[2] )
		{
			return new CBomb ( params.first , cost , hp , dmg , as , ar );
		}
		else if ( type == TOWER_TYPES[3] )
		{
			return new CSupport ( params.first , cost , hp , dmg , as , ar );
		}
		return nullptr;
}

CTower * CTowerMaker::constructTower ( std::pair < std::string, std::tuple<std::string, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t> > params,
																			 std::tuple < uint8_t , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t > modifers  )
{
		std::string type;
		uint8_t cost, hp, dmg, as, ar , curHp, atup, atupTime, asup, asupTime, energy;
		std::tie ( type, cost, hp, dmg, as, ar ) = params.second;
		std::tie ( energy , curHp, atup , atupTime, asup, asupTime ) = modifers;
		
		if ( type == TOWER_TYPES[0] )
		{
			return new CTower ( params.first , cost , hp , curHp , dmg , as , ar , energy , atup , atupTime , asup , asupTime );
		}
		else if ( type == TOWER_TYPES[1] )
		{
			return new CMelee ( params.first , cost , hp , curHp , dmg , as , 1 , energy , atup , atupTime , asup , asupTime );
		}
		
		else if ( type == TOWER_TYPES[2] )
		{
			return new CBomb ( params.first , cost , hp , curHp , dmg , as , ar , energy , atup , atupTime , asup , asupTime );
		}
		else if ( type == TOWER_TYPES[3] )
		{
		return new CSupport ( params.first , cost , hp , curHp , dmg , as , ar , energy , atup , atupTime , asup , asupTime );
		}
		return nullptr;
}

CTowerMaker::CTowerMaker ( void ){}
