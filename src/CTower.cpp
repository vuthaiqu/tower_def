#include "CTower.h"

CTower::CTower ( const std::string & name , uint8_t cost , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range )
	: m_name ( name ),
		m_cost ( cost ),
		m_maxHp ( hp ),
		m_hp ( hp ),
		m_range ( range ),
		m_dmg ( dmg ),
		m_attackSpeed ( as ),
		m_energy ( 0 ),
		m_attackBuff ( 0 ),
		m_attackBuffDuration ( 0 ),
		m_attackSpeedBuff ( 0 ),
		m_attackSpeedBuffDuration ( 0 )
{

}

CTower::CTower ( const std::string & name , uint8_t cost , uint8_t maxHp , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range , uint8_t enrgy, 
							uint8_t atup , uint8_t atupTime , uint8_t asup , uint8_t asupTime )
	: m_name ( name ),
		m_cost ( cost ),
		m_maxHp ( maxHp ),
		m_hp ( hp ),
		m_range ( range ),
		m_dmg ( dmg ),
		m_attackSpeed ( as ),
		m_energy ( enrgy ),
		m_attackBuff ( atup ),
		m_attackBuffDuration ( atupTime ),
		m_attackSpeedBuff ( asup ),
		m_attackSpeedBuffDuration ( asupTime )
{
	
}


CTower::~CTower ( void )
{}

std::pair < std::pair < uint8_t , uint8_t > , uint8_t > CTower::attackEnemies ( void ) const
{
	if ( m_hp != 0 )
		return std::make_pair ( std::make_pair ( m_attackBuff + m_dmg , 1 ) , m_range );
	else return std::make_pair ( std::make_pair ( 0 , 0 ) , 0 );
}

std::pair < std:: pair < std::tuple < uint8_t , uint8_t , uint8_t > , uint8_t > , uint8_t > CTower::buffTowers ( void ) const
{
	return std::make_pair ( std::make_pair ( std::make_tuple ( 0 , 0 , 0 ) , 0 ) , 0 );
}

#include <fstream>
bool CTower::charge ( void )
{
	bool retval;
	if ( m_attackBuffDuration == 0 )
		m_attackBuff = 0;

	if ( m_attackSpeedBuffDuration == 0 ) 
		m_attackSpeedBuff = 0;

	if ( m_attackSpeed + m_attackSpeedBuff != 0 &&  m_energy == 60 / ( m_attackSpeed + m_attackSpeedBuff )  )
	{
		m_energy = 0;
		retval = true;
	}
	else
	{
		++ m_energy;
		retval = false;
	}
	return retval;
}

bool CTower::takeDamge ( uint8_t dmg )
{
	if ( m_hp <= dmg )
	{
		m_hp = 0;
		return true;
	}
	m_hp -= dmg;
	return false;
}

std::string CTower::drawIcon ( void ) const
{
	return "=/\\";
}

std::string CTower::drawHp ( void ) const
{
	return std::to_string ( m_hp );
}


CTower * CTower::clone ( void ) const
{
	return new CTower ( *this );
}

std::string CTower::getName ( void ) const
{
	return m_name;
}

uint8_t CTower::getCost ( void ) const
{
	return m_cost;
}

bool CTower::hillPlacable ( void ) const
{
	return true;
}

void CTower::getBuff ( std::pair < std::tuple < uint8_t , uint8_t , uint8_t > , uint8_t > buff )
{
	uint8_t heal, atup, asup;
	std::tie ( heal , atup , asup ) = buff.first;
	if ( heal + m_hp > m_maxHp )
	{
		m_hp = m_maxHp;
	}
	else
		m_hp += heal;
	if ( atup > m_attackBuff )
	{
		m_attackBuff = atup;
		m_attackBuffDuration = buff.second;
	}
	else if ( atup == m_attackBuff )
		m_attackBuffDuration = buff.second;

	if ( asup > m_attackSpeedBuff )
	{
		m_attackSpeedBuff = asup;
		m_attackSpeedBuffDuration = buff.second;
	}
	else if ( asup == m_attackSpeedBuff )
		m_attackSpeedBuffDuration = buff.second;
}
//P_BUFF_TIME ATTACK_SPEEDUP ASUP_BUFF_TIME
std::string CTower::print ( void ) const
{
	std::string retval;
	retval.append( m_name + " " );
	retval.append( std::to_string ( m_hp ) + " " );
	retval.append( std::to_string ( m_energy ) + " " );
	retval.append( std::to_string ( m_attackBuff ) + " " );
	retval.append( std::to_string ( m_attackBuffDuration ) + " " );
	retval.append( std::to_string ( m_attackSpeedBuff ) + " "  );
	retval.append( std::to_string ( m_attackSpeedBuffDuration )  );
	return retval;
}

