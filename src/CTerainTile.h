#include <list>
#include "CTile.h"



/**
 * @brief Terrain tile, some turrets can be placed on this tile some cannot
 */
class CTerainTile : public CTile
{
  public:
		/**
		 * @brief Construct a new CTerainTile object
		 */
    CTerainTile( void );
		/**
		 * @brief Returns savable representaion of the tile
		 * 
		 * @return 'T'
		 */
    const char saveChar ( void ) const override;
    bool canBeBuild ( bool canBePlacedOnTerrain ) const override;

};
