#include "CSpawnTile.h"

CSpawnTile::CSpawnTile()
	: CTile()
{
	m_passeble = true;
	m_isTerrian = false;
	m_tileRepresentation.addRow( " ___ " )
											.addRow( " | | " )
											.addRow( " ooo " );
}

const char CSpawnTile::saveChar ( void ) const
{
	return 'S';
}

bool CSpawnTile::canBeBuild ( bool canBePlacedOnTerrain ) const
{
	return false;
}