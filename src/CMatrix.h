#pragma once

#include "CTower.h"
#include <memory>
#include <vector>
#include <string>


/**
 * @brief Char representation of one cell on the field
 */
class CMatrix
{
	public:
		/**
	 	* @brief Construct a new CMatrix object
	 	* 
	 	*/
	  CMatrix ();
	  /**
	   * @brief Get the string representation of one row
	   * 
	   * @param row number of row to get.
		 * @return row if number of row is less then height of matrix
	   */
		std::string getRow ( uint8_t row ) const;
		/**
		 * @brief Add row to the matrix
		 * 
		 * @param row string row to be added. Valid row cannot be longer than value in m_width or adding would ressult matrix being higher than m_height 
		 * @return self reference
		 */
	  CMatrix & addRow ( const std::string & row );
		/**
		 * @brief add vissual representation of the cursor
		 * 
		 * @return self reference
		 */
		CMatrix & addCursor ( void );
		/**
		 * @brief Get the Width object
		 */
	  static uint8_t getWidth ( void );
		/**
		 * @brief Get the Height object
		 */
	  static uint8_t getHeight ( void );
		/**
		 * @brief Add vissual representation for tower in first two rows
		 * 
		 * @param tower tower to be drawn
		 * @return self reference
		 */
		CMatrix & drawTower ( std::weak_ptr < CTower > tower );
		/**
		 * @brief Add vissual representation for enemy in first two rows
		 * 
		 * @param enemy enemy to be drawn 
		 * @return self reference
		 */
		CMatrix & drawTwoRows ( std::string first, std::string second );

	private:
	  static const uint8_t m_width;
	  static const uint8_t m_height;
	  std::vector < std::string > m_matrix;
};
