#pragma once

#include <fstream>
#include <string>
#include <map>
#include <set>
#include <utility>

#include "CTowerMaker.h"


/**
 * @brief Config loader. Loads configuration of towers and enenmies
 * 
 */
class CConfLoader
{
	public:
		/**
		 * @brief Construct a new CConfLoader object
		 * 
		 * @param enemiesTypes type of enemies that can be initialized into game
		 * @param turretsTypes type of turrets that can be initialized into game
		 */
		CConfLoader( void );
		/**
		 * @brief Get the enemy conf
		 * 
		 * @return map indexed by enemy name of tuples containing type loot hp dmg attack_speed attack_range movement_inteligence movement_speed in order
		 */
		const std::map < 	std::string ,
								std::tuple < std::string , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t > > & getEnemyConf ( void ) const;
		/**
		 * @brief Get the tower conf
		 * 
		 * @return map indexed by tower name of tuples containing type cost hp dmg attack_speed attack_range in order
		 */		
		const std::map < 	std::string , 
								std::tuple < std::string , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t> > & getTowerConf ( void ) const;
		operator bool ( void );						
	private:
		bool loadEnemyConf ( const std::set < std::string > & enemiesTypes );
		bool loadTowerConf (  const std::set < std::string > & towerTypes );
		std::map < 	std::string ,
								std::tuple < std::string , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t > > m_enemyConf;
		std::map < 	std::string ,
								std::tuple < std::string , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t > > m_towerConf;
		static const std::string CONF_FILE;
		std::ifstream m_ifs;
		bool m_goodBit;
};

