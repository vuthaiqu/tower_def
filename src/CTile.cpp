#include "CTile.h"

CTile::CTile()
	:	m_passeble ( true ),
		m_isTerrian ( false )
{}

CTile::~CTile()
{}

CMatrix CTile::getMatrix () const
{
	return m_tileRepresentation;
}

bool CTile::isPasseble () const
{
	return m_passeble;
}