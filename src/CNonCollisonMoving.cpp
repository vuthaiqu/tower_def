#include "CNonCollisonMoving.h"

CNonCollisonMoving::CNonCollisonMoving(  std::shared_ptr < const CMap > map )
	: CMove ( map )
{
}

std::pair < uint8_t , uint8_t > CNonCollisonMoving::move ( std::pair < uint8_t , uint8_t > from , std::shared_ptr < CTower > ** towers )
{
	if ( m_path.size() == 0 )
	{
		for ( uint8_t i = 0 ; i < m_mapHeight ; ++ i )
			for ( uint8_t j = 0 ; j < m_mapWidth ; ++ j )
				if ( towers[i][j] != nullptr )
					m_map[i][j].first = 'T';
		if ( ! createPath( from ) )
		{
			for ( uint8_t i = 0 ; i < m_mapHeight ; ++ i )
				for ( uint8_t j = 0 ; j < m_mapWidth ; ++ j )
					if ( towers[i][j] != nullptr )
						m_map[i][j].first = ' ';
			if ( ! createPath ( from ) )
			{
				return from;
			}
		}
	}
	std::pair < uint8_t , uint8_t > retval = m_path.front();
	m_path.pop_front();
	return retval;
	
}