#pragma once

#include <tuple>
#include <utility>
#include <cstdint>
#include <string>

class CTower
{
	public:
		/**
		 * @brief Construct a new CTower object
		 * 
		 * @param name name of tower
		 * @param cost cost of the tower
		 * @param hp health points
		 * @param dmg dmg of the tower
		 * @param as attack speed of the tower
		 * @param range range of the tower
		 */
		CTower ( const std::string & name , uint8_t cost , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range );
		/**
		 * @brief Construct a new CTower object
		 * 
		 * @param name name of tower
		 * @param cost cost of the tower
		 * @param maxHp health points
		 * @param hp health points
		 * @param dmg dmg if the tower
		 * @param as attack speed of the tower
		 * @param range range of the tower
		 * @param energy energy to attack
		 * @param atup attack up
		 * @param atupTime attack up durration
		 * @param asup attack speed up
		 * @param asupTime attack speed up durration
		 */
		CTower ( const std::string & name , uint8_t cost , uint8_t maxHp , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range , uint8_t energy,
							uint8_t atup , uint8_t atupTime , uint8_t asup , uint8_t asupTime );
		virtual ~CTower ( void );

		/**
		 * @brief Charge tower for next attack
		 * 
		 * @return true if tower is ready to attack, false otherwise
		 */
		bool charge ( void );
		/**
		 * @brief Turrets takes dmg
		 * 
		 * @param dmg damge to be taken
		 * @return true if towers health points reaches 0, false otherwise
		 */
		bool takeDamge ( uint8_t dmg );
		/**
		 * @brief Get cost of the tower
		 */
		uint8_t getCost ( void ) const;
		/**
		 * @brief Get the Name object
		 */
		std::string getName ( void ) const;
		/**
		 * @brief Draws tower hp in string
		 */
		std::string drawHp ( void ) const;
		/**
		 * @brief Get the Buff object
		 */
		virtual void getBuff ( std::pair < std::tuple < uint8_t , uint8_t , uint8_t > , uint8_t > buff );
		/**
		 * @brief Prints savable data
		 */
		virtual std::string print ( void ) const;
		/**
		 * @brief Return the attack on enemies 
		*/
		virtual std::pair < std::pair < uint8_t , uint8_t > , uint8_t > attackEnemies ( void ) const;
		/**
		 * @brief Return buff on towers
		 */
		virtual std::pair < std:: pair < std::tuple < uint8_t , uint8_t , uint8_t > , uint8_t > , uint8_t > buffTowers ( void ) const;
		/**
		 * @brief Retuns if tower can be placed on hill
		 */
		virtual bool hillPlacable ( void ) const;
		/**
		 * @brief Draws icon
		 */
		virtual std::string drawIcon ( void ) const;
		/**
		 * @brief Returns dynamicaly allocated copy of self
		 */
		virtual CTower * clone ( void ) const;

	protected:
	 	std::string m_name;
		uint8_t m_cost;
		
		uint8_t m_maxHp;
		uint8_t m_hp;

		uint8_t m_range;
		uint8_t m_dmg;
		uint8_t m_attackSpeed;
		uint8_t m_energy;

		uint8_t m_attackBuff;
		uint8_t m_attackBuffDuration;

		uint8_t m_attackSpeedBuff;
		uint8_t m_attackSpeedBuffDuration;
};