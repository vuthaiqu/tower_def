#include "CSaves.h"

const std::string CSaves::m_savedGamesDir = "./examples/saved_games/";

CSaves::CSaves( std::shared_ptr < CScreen > prevScreen )
	: CScreen ( prevScreen ),
		m_selection( 0 ),
		m_missingFile ( false )
{
	if ( std::filesystem::exists( m_savedGamesDir ) ) 
	{
	for ( const auto & entry :  std::filesystem::directory_iterator( m_savedGamesDir ) )
		m_savedGames.push_back ( entry.path().filename().string() );
	} else m_missingFile = true;
}

std::shared_ptr < CScreen > CSaves::active ( std::shared_ptr < CScreen > self )
{
	int ch = getch();
	erase();
	if ( m_missingFile )
	{
		printw("FILE CORRUPTION\nPRESS ANY KEY TO EXIT");
		if ( ch != -1 ) return nullptr;
		return self;
	}
	if ( m_savedGames.size() == 0 )
		printw ( "NO SAVED GAME\n" );
	if ( ch == '\n' )
		return std::shared_ptr < CScreen > ( new CGameScreen ( nullptr , m_savedGamesDir + m_savedGames [ m_selection ] ) );
	if ( ch == KEY_BACKSPACE )
		return m_prevScreen;

	if ( ch == KEY_UP )
		m_selection = ( m_selection + m_savedGames.size() - 1 ) %  m_savedGames.size() ; 
	if ( ch == KEY_DOWN )
		m_selection = ( m_selection + 1 ) % m_savedGames.size();

	std::string line;
	for ( size_t i = 0 ; i < m_savedGames.size() ; i ++ )
	{
		if ( m_selection == i )
			line = "[" + m_savedGames[i] + "]\n";
		else
			line = " " + m_savedGames[i] + "\n";
		printw ( line.c_str() );
	}
	return self;
}
