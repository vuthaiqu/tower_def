#include <cassert>
#include <fstream>
#include "CSaveLoader.h"

CSaveLoader::CSaveLoader ( const std::string & fileName , const std::shared_ptr < CConfLoader > conf)
	: m_ifs ( fileName ),
		m_conf ( conf ),
		m_goodBit ( true )
{
	
}

CSaveLoader::operator bool ( void ) const
{
	return m_goodBit;
}

std::unique_ptr < CGame > CSaveLoader::loadGame ( int refreshSpeed )
{
	m_goodBit = true;
	if ( ! m_ifs )
		return nullptr;
	uint32_t money = loadMoney ();
	if ( ! m_goodBit || ! m_ifs  )
		return nullptr;
	uint8_t hp = loadHp ();
	if ( ! m_goodBit || ! m_ifs )
		return nullptr;
	std::shared_ptr < CMap > map = loadMap ();
	if ( ! m_goodBit || ! m_ifs || ! ( * map ) )
	{
		m_goodBit = false;
		return nullptr;
	}
	std::vector < std::pair < uint8_t , uint8_t > > starts = map->getSpawnPoints();
	for ( std::pair < uint8_t , uint8_t > start : starts )
	{
		CMove reachableTest ( map );
		if ( ! reachableTest.endReachable( start ) )
		{
			m_goodBit = false;
			return nullptr;
		}
	}
	std::unique_ptr < CGame > game ( new CGame ( map  , money , hp , m_conf , refreshSpeed ) );
	std::map < std::pair < uint8_t , uint8_t > , std::shared_ptr < CTower > > towers = loadTowers();
	if ( ! m_goodBit || ! m_ifs )
		return nullptr;
	
	for ( std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CTower > > read : towers )
	{
		if ( ! game->setTowerTo ( read.second , read.first )  )
		{
			m_goodBit = false;
			return nullptr;
		}
	}
	std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CEnemy > > > enemies = loadEnemyOnMap ( map );
	if ( ! m_goodBit || ! m_ifs )
		return nullptr;
	for ( std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CEnemy > > read : enemies )
	{
		if ( ! game->setEnemyTo ( read.second , read.first ) )
		{
			m_goodBit = false;
			return nullptr;
		}
	}
	std::queue < std::shared_ptr < CEnemy > > queue = loadEnemyQueue( map );
	if ( ! m_goodBit || ! m_ifs )
		return nullptr;
	game->setQueue ( queue );
	m_ifs.get();
	if ( ! m_ifs.eof() )
	{
		m_goodBit = false;
		return nullptr;		
	}
	return game;
}

std::unique_ptr<CMap> CSaveLoader::loadMap ( void )
{
	std::vector < std::string > charMap;
	std::string line;
	int height, width;
	char tmpChar;
	m_ifs >> line >> height >> width >> tmpChar;
	if ( line != "<Map" || tmpChar != '>' || height <= 0 || height > UINT8_MAX || width <= 0 || width > UINT8_MAX || ! m_ifs )
	{
		m_goodBit = false;
		return nullptr;
	}

	for ( int i = 0 ; i < height ; i ++ )
	{
		m_ifs >> line;
		if ( ! m_ifs || line.size() != ( unsigned ) width )
		{
			m_goodBit = false;
			return nullptr;
		}
		charMap.emplace_back ( line );
	}

	std::unique_ptr < CMap > map ( new CMap ( width , height , charMap ) );
	return map;
}

std::queue < std::shared_ptr < CEnemy > > CSaveLoader::loadEnemyQueue ( std::shared_ptr < CMap > map )
{
	std::queue < std::shared_ptr < CEnemy > > queue;
	std::string line;
	int number;
	char tmpChar;
	m_ifs >> line >> number >> tmpChar;
	if ( line != "<Enemy_Queue" || number < 0 || tmpChar != '>' )
	{
		m_goodBit = false;
		return queue;
	}
	for ( int i = 0 ; i < number ; i ++ )
	{
		m_ifs >> line;
		auto tmp = m_conf->getEnemyConf().find( line );
		if ( ! m_ifs || tmp == m_conf->getEnemyConf().end() )
		{
			m_goodBit = false;
			return queue;
		}

		uint8_t loot, hp, dmg, as, ar, iq, ms;
		std::string TYPE;
		std::tie ( TYPE , loot, hp, dmg, as, ar, iq, ms ) = tmp->second;
		queue.push ( std::shared_ptr < CEnemy > ( new CEnemy ( line , loot , hp , dmg , as , ar , ms , iq , map ) ) );
	}
	return queue;
}

std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CEnemy > > > CSaveLoader::loadEnemyOnMap ( std::shared_ptr < CMap > map )
{
	std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CEnemy > > > list;
	std::string line;
	int number;
	char tmpChar;
	m_ifs >> line >> number >> tmpChar;
	if ( line != "<Enemy_List" || number < 0 || tmpChar != '>' || ! m_ifs )
	{
		m_goodBit = false;
		return list;
	}
	int xcord, ycord, curHp, charge, energy, slow, slowDuration;
	std::string NAME;
	for ( int i = 0 ; i < number ; i ++ )
	{
		m_ifs >> ycord >> xcord >> NAME >> curHp >> charge >> energy >> slow >> slowDuration;
		auto enemyData = m_conf->getEnemyConf().find(NAME);
		if ( 	! m_ifs ||
					xcord < 0 || xcord > UINT8_MAX ||
					ycord < 0 || ycord > UINT8_MAX ||
					curHp <= 0 || curHp > UINT8_MAX ||
					energy < 0 || energy > UINT8_MAX ||
					slow < 0 || slow > UINT8_MAX ||
					slowDuration < 0 || slowDuration > UINT8_MAX ||
					enemyData == m_conf->getEnemyConf().end() )
		{
			m_goodBit = false;
			return list;
		}
		uint8_t loot, hp, dmg, attackSpeed, attackRange, iq, ms;
		std::string type;
		std::tie ( type , loot, hp, dmg, attackSpeed, attackRange, iq, ms ) = enemyData->second;
		list.push_back ( std::pair ( std::pair ( ycord , xcord ) , 
											std::shared_ptr < CEnemy > (
												new CEnemy ( NAME , loot , hp , dmg , attackSpeed , attackRange , charge , ms , energy , iq , slow , slowDuration , map ) ) ) );
	}
	return list;
}



std::map < std::pair < uint8_t , uint8_t > , std::shared_ptr < CTower > > CSaveLoader::loadTowers ( void )
{
	std::map < std::pair < uint8_t , uint8_t > , std::shared_ptr < CTower > > towers;
	std::string line;
	int number;
	char tmpChar;
	m_ifs >> line >> number >> tmpChar;
	if ( line != "<Towers" || number < 0 || tmpChar != '>' || ! m_ifs )
	{
		m_goodBit = false;
		return towers;
	}
	int xcord, ycord, hp, charge, atup, atupTime, asup, asupTime;
	uint8_t cost, maxhp, dmg, as, ar;
	std::string NAME, TYPE;
	for ( int i = 0 ; i < number ; i ++ )
	{
		m_ifs >> ycord >> xcord >> NAME >> hp >> charge >> atup  >> atupTime >> asup >> asupTime ;
		auto confData = m_conf->getTowerConf().find( NAME );
		auto saveData = towers.find ( std::make_pair ( ycord , xcord ) );
		if (	! m_ifs ||
					xcord < 0 || xcord > UINT8_MAX ||
					ycord < 0 || ycord > UINT8_MAX ||
					hp <= 0  || hp > UINT8_MAX ||
					charge < 0 || charge > UINT8_MAX ||
					atup < 0 || atup > UINT8_MAX ||
					atupTime < 0 || atupTime > UINT8_MAX ||
					asup < 0 || asup > UINT8_MAX ||
					asupTime  < 0 || asupTime  > UINT8_MAX ||
					confData == m_conf->getTowerConf().end() ||
					saveData != towers.end() )
		{
			m_goodBit = false;
			return towers;
		}
		std::tie ( TYPE, cost, maxhp, dmg, as, ar ) = confData->second;
		towers.emplace ( std::make_pair ( ycord , xcord ),
											CTowerMaker::constructTower (  
												std::make_pair (
													NAME ,
													std::make_tuple ( TYPE , cost , maxhp , dmg , as , ar ) ), 
												std::make_tuple ( charge, hp, atup ,atupTime , asup , asupTime  ) ) );
	}
	return towers;
}

uint32_t CSaveLoader::loadMoney ()
{
	std::string line;
	long money;
	char tmpChar;
	m_ifs >> line >> money >> tmpChar;
	if ( line != "<Money" || money < 0 || money > UINT32_MAX || tmpChar != '>' || ! m_ifs )
	{
		m_goodBit = false;
		return 0;
	}

	return ( uint32_t ) money;
}

uint8_t CSaveLoader::loadHp ()
{
	std::string line;
	int hp;
	char tmpChar;
	m_ifs >> line >> hp >> tmpChar;
	if ( line != "<HP" || hp < 0 || hp > UINT8_MAX || tmpChar != '>' || ! m_ifs )
	{
		m_goodBit = false;
		return 0;
	}

	return ( uint8_t ) hp;
}
