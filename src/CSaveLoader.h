#pragma once
#include <fstream>
#include <list>
#include <queue>
#include <map>
#include <set>
#include <string>

#include "CTowerMaker.h"
#include "CEnemy.h"
#include "CConfLoader.h"
#include "CMove.h"
#include "CGame.h"

/**
 * @brief Class responsible for loading saved or new games
 * 
 */
class CSaveLoader
{
  public:
		/**
		 * @brief Construct a new CSaveLoader object
		 * 
		 * @param fileName name of file where the game is saved and will be loaded from
		 * @param conf configuration of game objecs
		 */
		CSaveLoader ( const std::string & fileName , const std::shared_ptr < CConfLoader > conf );
		/**
		 * @brief loads game
		 * 
		 * @param refreshSpeed refresh speed of game windows
		 * @return unique pointer to game 
		 */
		std::unique_ptr < CGame > loadGame ( int refreshSpeed );
		/**
		 * @brief operator bool returns true, if game has been loaded successfully false otherwise 
		 */
		operator bool ( void ) const;
	private:
		std::unique_ptr < CMap > loadMap ( void );
		std::queue < std::shared_ptr < CEnemy > > loadEnemyQueue ( std::shared_ptr < CMap > map );
		std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CEnemy > > > loadEnemyOnMap ( std::shared_ptr < CMap > map );
		std::map < std::pair < uint8_t , uint8_t > , std::shared_ptr < CTower > > loadTowers ( void );
		uint32_t loadMoney ( void );
		uint8_t loadHp ( void );
		std::ifstream m_ifs;
		const std::shared_ptr < CConfLoader > m_conf;
		bool m_goodBit;
};
