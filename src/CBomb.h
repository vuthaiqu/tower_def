#include "CTower.h"


/**
 * @brief Bomb, when dies it explodes can be used to block, cannot be placed on terrain
 * 
 */
class CBomb : public CTower
{
	public:
	/**
	 * @brief Construct a new CBomb object. Mostly used for in game creation of new object
	 * 
	 * @param name name of tower
	 * @param cost cost for tower
	 * @param hp max health points of tower
	 * @param dmg explosion damage
	 * @param as attack speed of tower
	 * @param range range of explosion
	 */
		CBomb( const std::string & name , uint8_t cost , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range );

	/**
	 * @brief Construct a new CBomb object
	 * 
	 * @param name name of tower
	 * @param cost cost for tower
	 * @param maxHp max health points of tower
	 * @param hp curent health points of tower
	 * @param dmg explosion damage
	 * @param as attack speed of tower
	 * @param range range of explosion
	 * @param energy energy 
	 * @param atup attack up buff
	 * @param atupTime attack up buff durration
	 * @param asup attack speed up buff
	 * @param asupTime attack speed up buff 
	 */
		CBomb( const std::string & name , uint8_t cost , uint8_t maxHp , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range , uint8_t energy ,
						uint8_t atup , uint8_t atupTime , uint8_t asup , uint8_t asupTime );

//		std::string drawStats ( void ) const override;
//		std::string printInfo ( void ) const override;
		/**
		 * @brief returns attack
		 * 
		 * @return std::pair < std::pair < uint8_t , uint8_t > , uint8_t > first of pair is pair containing damge and number of targeted enememies and second contains range
		 */
		std::pair < std::pair < uint8_t , uint8_t > , uint8_t > attackEnemies ( void ) const override ;
		/**
		 * @brief apply buff from other towers
		 * 
		 * @param buff buff to be applied
		 */
		void getBuff ( std::pair < std::tuple < uint8_t , uint8_t , uint8_t > , uint8_t > buff ) override ;
		/**
		 * @brief Determines if tower can be plased on terrain. For this tower it is always false
		 */
		bool hillPlacable ( void ) const override;
		/**
		 * @brief Returns vissual representation of object
		 */
		std::string drawIcon ( void ) const override;
		/**
		 * @brief Returns new dynamicaly allocated instance of self copy
		 */		
		CTower * clone ( void ) const override;
};
