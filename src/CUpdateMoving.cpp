#include "CUpdateMoving.h"

CUpdateMoving::CUpdateMoving(  std::shared_ptr < const CMap > map )
	: CMove (  map )
{
}


std::pair < uint8_t , uint8_t > CUpdateMoving::move ( std::pair < uint8_t , uint8_t > from , std::shared_ptr  < CTower > ** towers )
{
	m_path.clear();
	if ( m_path.size() == 0 )
	{
		for ( uint8_t i = 0 ; i < m_mapHeight ; ++ i )
			for ( uint8_t j = 0 ; j < m_mapWidth ; ++ j )
				if ( towers[i][j] != nullptr && m_map[i][j].first != 'T' )
					m_map[i][j].first = 'O';
		if ( ! createPath ( from ) )
		{
			m_path.clear();
			for ( uint8_t i = 0 ; i < m_mapHeight ; ++ i )
				for ( uint8_t j = 0 ; j < m_mapWidth ; ++ j )
					if ( m_map[i][j].first == 'O' )
						m_map[i][j].first = ' ';
			if ( ! createPath ( from ) )
				return from;
		}
	}
	std::pair < uint8_t , uint8_t > retval = m_path.front();
	m_path.pop_front();
	return retval;

}




