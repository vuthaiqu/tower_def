#include "CSaveSaver.h"
#include "CScreen.h"

/**
 * @brief Class responsible for drawing saving screen
 */
class CSavingScreen : public CScreen
{
	public:
		/**
		 * @brief Construct a new CSavingScreen object
		 * 
		 * @param prevScreen previous screen
		 * @param game game to be saved
		 */
		CSavingScreen( std::shared_ptr < CScreen > prevScreen , const std::unique_ptr<CGame> & game );
		/**
		 * @brief Draws the screen, exits if game is saved
		 * 
		 * @param prev 
		 * @return nullptr if game is saved, previous screen if backspaced, this screen otherwise
		 */
		std::shared_ptr < CScreen > active ( std::shared_ptr < CScreen > prev );
	private:
		static const std::string SAVE_DIRECTORY;
		const std::unique_ptr<CGame> & m_game;
		std::string m_saveName;
		bool m_saved;
		bool m_err;
};