#include "CMove.h"
/**
 * @brief Class responsible for moving. Class calculates optimal path only once.
 * Uses algorythm that tries to avoid enemy towers. If no goal is reachable it doesnt stops avoiding them
 */
class CNonCollisonMoving : public CMove
{
	public:
		/**
		 * @brief Construct a new CNonCollisonMoving object
		 * 
		 * @param map map where path should be found
		 */
		CNonCollisonMoving( std::shared_ptr < const CMap > map );
		/**
		 * @brief Construct new path and returns next move.
		 * 
		 * @param from position from which is path calculated
		 * @param towers map of twers in way
		 * @return std::pair < uint8_t , uint8_t > 
		 */
		std::pair < uint8_t , uint8_t > move ( std::pair < uint8_t , uint8_t > from , std::shared_ptr < CTower > ** towers ) override;
};
