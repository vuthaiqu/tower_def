#pragma once
#include "CMatrix.h"


/**
 * @brief Abstract class of tiles
 * 
 */
class CTile
{
  public:
		/**
		 * @brief Construct a new CTile object
		 */
    CTile();
		/**
		 * @brief Destroy the CTile object
		 */
    virtual ~CTile();
		/**
		 * @brief Returns the Matrix vissual representation of tile
		 */
    CMatrix getMatrix () const;
		/**
		 * @brief Returns if enemies can pass through this tile. Treu if they can, false otherwise
		 */
		bool isPasseble ( void ) const;
		/**
		 * @brief 
		 * 
		 * @return const char 
		 */
    virtual const char saveChar ( void ) const = 0;
    virtual bool canBeBuild ( bool canBePlacedOnTerrain ) const = 0;
  protected:
    bool m_passeble;
    bool m_isTerrian;
    CMatrix m_tileRepresentation;
};