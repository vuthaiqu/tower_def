#pragma once

#include <cstdint>
#include <utility>
#include <memory>
#include <list>
#include <queue>

#include "CMap.h"
#include "CTower.h"


/**
 * @brief Class responsible for moving. Uses BFS for calculating optimal path, doesnt look at builded obstacles
 * 
 */
class CMove
{
	public:
		/**
		 * @brief Construct a new CMove object
		 * 
		 * @param map map where the path should be found
		 */
		CMove( std::shared_ptr < const CMap > map );
		/**
		 * @brief Destroy the CMove object
		 * 
		 */
		virtual ~CMove();
		/**
		 * @brief Calculates path and moves, for this class it calculates only for first move
		 * 
		 * @param from starting possition
		 * @param towers map of towers
		 * @return next move in path
		 */
		virtual std::pair < uint8_t , uint8_t > move ( std::pair < uint8_t , uint8_t > from , std::shared_ptr  < CTower > ** towers );
		/**
		 * @brief Decides if the end is reachable from the position
		 * 
		 * @param from position from where it is calculated
		 * @return true if any goal is reachable from the position, false otherwise
		 */
		bool endReachable ( std::pair < uint8_t , uint8_t > from );
	protected:
		bool createPath ( std::pair < uint8_t , uint8_t > from );
		void inspectNeigbors ( std::pair < uint8_t , uint8_t > active , std::queue < std::pair < uint8_t , uint8_t > > & queue );
		void constructPath ( std::pair < uint8_t , uint8_t > active , std::pair < uint8_t , uint8_t > start  );
		std::list < std::pair < uint8_t	 , uint8_t > > m_path;
		std::pair < char , uint32_t > ** m_map;
		uint8_t m_mapHeight;
		uint8_t m_mapWidth;
};
