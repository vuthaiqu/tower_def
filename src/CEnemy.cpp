#include "CEnemy.h"
#include <fstream>

CEnemy::CEnemy ( const std::string & name , uint8_t money , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range , uint8_t speed , uint8_t iq ,
								std::shared_ptr < const CMap > map )
	: CEnemy ( name , money , hp , dmg , as , range , 0 , speed , 0 , iq , 0 , 0 , map )
{
}

CEnemy::CEnemy ( const std::string & name , uint8_t money , uint8_t hp , uint8_t dmg , uint8_t as , uint8_t range ,
				uint8_t charge , uint8_t speed , uint8_t energy , uint8_t iq , uint8_t slow , uint8_t slowDuration , std::shared_ptr < const CMap > map )
	: m_name ( name ),
		m_hp ( hp ),
		m_money ( money ),
		m_dmg ( dmg ),
		m_attackSpeed ( as ),
		m_attackRange ( range ),
		m_charge ( 0 ),
		m_speed ( speed ),
		m_energy ( energy ),
		m_movementInteligence ( iq ),
		m_slow ( slow ),
		m_slowDuration ( slowDuration ),
		m_moved ( false )
{
	if ( m_movementInteligence == 0 )
		m_move = std::make_unique < CMove > ( map );
	else if ( m_movementInteligence == 1 )
		m_move = std::make_unique < CNonCollisonMoving > ( map );		
	else if ( m_movementInteligence == 2 )
		m_move = std::make_unique < CUpdateMoving > ( map );
}

CEnemy::~CEnemy( void )
{
	
}

uint8_t CEnemy::getMoney ( void ) const
{
	return m_money;
}
/*
std::pair < uint8_t , uint8_t > CEnemy::attackEnemies ( void )
{
	return std::make_pair ( m_dmg , m_attackRange );
}
*/
uint8_t CEnemy::attackTowers ( void ) const
{
	return m_dmg;
}

std::pair < uint8_t , uint8_t > CEnemy::tryToMove ( std::pair < uint8_t , uint8_t > cord , std::shared_ptr <CTower> ** towers )
{
	if ( ! m_moved )
	{
		m_nextMove = m_move->move( cord , towers );
		m_moved = true;
	}

	if ( m_nextMove != cord )
	{
		return m_nextMove;
	}
	 
	if ( m_energy >= 60 / m_speed )
	{
		m_nextMove = m_move->move( cord , towers );
		m_energy = 0;
		return m_nextMove;
	}
	++ m_energy;
	return cord;
}

bool CEnemy::takeDmg ( uint8_t dmg )
{
	if ( m_hp <= dmg )
	{
		m_hp = 0;
		return true;
	}
	m_hp -= dmg;
	return false;
}

const std::string & CEnemy::getName ( void ) const
{
	return m_name;
}

std::string CEnemy::getIcon ( void ) const
{
	return "\\0/";
}


uint8_t CEnemy::getHp ( void ) const
{
	return m_hp;
}

bool CEnemy::charge ( void )
{
	if ( m_charge == 60 / m_attackSpeed )
	{
		m_charge = 0;
		return true;
	}
	else 
	{
		++ m_charge;
		return false;
	}
}

std::string CEnemy::print ( void ) const
{
	std::string retval;
	retval.append ( m_name + " " );
	retval.append ( std::to_string ( m_hp ) + " " );
	retval.append ( std::to_string ( m_charge ) + " " ); 
	retval.append ( std::to_string ( m_energy ) + " " ); 
	retval.append ( std::to_string ( m_slow ) + " " ); 
	retval.append ( std::to_string ( m_slowDuration ) ); 
	return retval;
}