#pragma once

#include "CScreen.h"
#include "CNewGame.h"
#include "CSaves.h"


/**
 * @brief Class responsible for game menu
 */
class CMenu : public CScreen
{
	public:
		/**
		 * @brief Construct a new CMenu object
		 * 
		 * @param prevScreen smart ptr to previous screen 
		 */
		CMenu ( std::shared_ptr < CScreen > prevScreen );
		/**
		 * @brief Draws menu on screen
		 * 
		 * @param self 
		 * @return next screen 
		 */
		std::shared_ptr < CScreen > active ( std::shared_ptr < CScreen > self ) override;
	private:
		uint8_t m_selection;
		const static std::string m_options [3] ;
};
