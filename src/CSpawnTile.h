#pragma once

#include "CTile.h"

/**
 * @brief Tile where enemies spawn
 * 
 */
class CSpawnTile : public CTile
{
	public:
		/**
		 * @brief Construct a new CSpawnTile object
		 */
		CSpawnTile();
		/**
		 * @brief Returns savable representaion of the tile
		 * @return 'S'
		 */
    const char saveChar ( void ) const override;
		/**
		 * @brief Returns false
		 */
    bool canBeBuild ( bool canBePlacedOnTerrain ) const override;
};
