#pragma once

#include <list>
#include <utility>
#include <memory>
#include <vector>
#include <queue>

#include "CEnemy.h"
#include "CTower.h"
#include "CTowerMaker.h"
#include "CMatrix.h"
#include "CConfLoader.h"
#include "CMap.h"


/**
 * @brief Game class. Runs the game
 * 
 */
class CGame
{
  public:
	/**
	 * @brief Construct a new CGame object
	 * 
	 * @param map map of the game
	 * @param money starting money
	 * @param hp starting heath points
	 * @param conf configuration for enemies and towers
	 * @param updateRate how many ms the screens update
	 */
    CGame ( std::shared_ptr < CMap > map , uint32_t money , uint8_t hp , const std::shared_ptr < CConfLoader > conf , uint8_t updateRate );
		/**
		 * @brief Destroy the CGame object
		 * 
		 */
    ~CGame ();
		/**
		 * @brief Set the Tower to y x coordinates on map
		 * 
		 * @param tower tower to be placed
		 * @param pos possition to be placed on
		 * @return false if enemy is on the tile or could not be build on the terrain, true otherwise 
		 */
    bool setTowerTo ( std::shared_ptr < CTower > tower , std::pair < uint8_t , uint8_t > pos );
		/**
		 * @brief Set the Enemy to y x coordinates on map
		 * 
		 * @param enemy enemy to be placed
		 * @param pos possition to be placed on
		 * @return false if tower is already on tile or the terrains is impassible, true otherwise
		 */
    bool setEnemyTo ( std::shared_ptr < CEnemy > enemy , std::pair < uint8_t , uint8_t > pos );
		/**
		 * @brief innits enemy queue
		 * 
		 * @param queue enemy queue to be set
		 * @return true 
		 */
    void setQueue ( const std::queue < std::shared_ptr < CEnemy > > & queue );
		/**
		 * @brief builds avaible tower on key input on cursor
		 * 
		 * @param ch kez input
		 */
		void buildOnInput ( int ch );
		/**
		 * @brief Does one tick of enemies and towers, also spawns new enemies if queue is not empty
		 */
		void tick ( void );
		/**
		 * @brief Get the Game display data in vector of strings. Every string in vector represents one line
		 * 
		 * @return Game display data
		 */
		std::vector < std::string > getGameDisplay ( void ) const;
		/**
		 * @brief Operator bool retrurns false if playe health points have reached zero, otherwise returns true
		 */
		operator bool ( void ) const;
		/**
		 * @brief Moves cursor to the left
		 */
		void moveLeft ( void );
		/**
		 * @brief Moves cursor to the right
		 */
		void moveRight ( void );
		/**
		 * @brief Moves cursor up
		 */
		void moveUp ( void );
		/**
		 * @brief Moves cursor down
		 */
		void moveDown ( void );
		/**
		 * @brief destroyes tower on cursor
		 * 
		 * @return reference to self
		 */
		CGame & destroyTower ( void );
		/**
		 * @brief Get reference to map
		 * 
		 * @return reference to map
		 */
		const CMap & getMap ( void ) const;
		/**
		 * @brief Get towers on map
		 * 
		 * @return list of pairs fisrt is pairs of y x coordinates of the tower, second is tower itself
		 */
		std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CTower > > > getTowers ( void ) const;
		/**
		 * @brief Get enemies on map
		 * 
		 * @return list of pair first is pairs of y x coordinates of the enemy, second is enemy itself
		 */
	  std::list < std::pair < std::pair < uint8_t , uint8_t > , std::shared_ptr < CEnemy > > > getEnemyOnMap ( void ) const;
		/**
		 * @brief Get spawn queue
		 * 
		 * @return enemy spawn queue 
		 */
		std::queue < std::shared_ptr < CEnemy > > getEnemyQueue () const;	
		/**
		 * @brief Returns player bank balance
		 */
		uint32_t getMoney ( void ) const;
		/**
		 * @brief Return player health points
		 */
		uint8_t getPlayerHp ( void ) const;

  private:
		std::string getBuildOptions ( void ) const;
		std::vector < std::unique_ptr < CTower > > m_towerModels;
		void buildModels ();

		void enemyPhase  ();
		void towerPhase  ();
		void checkEnemyOnGoal ();
		
		std::list < std::shared_ptr< CEnemy > > getEnemyInRadius ( uint8_t radius , std::pair < uint8_t , uint8_t > center ) const;
		std::list < std::shared_ptr< CTower > > getTowerInRadius ( uint8_t radius , std::pair < uint8_t , uint8_t > center ) const;

    std::shared_ptr < CMap > m_map;
    uint32_t m_money;
    uint8_t m_playerHp;
		const std::shared_ptr < CConfLoader > m_conf;
		std::pair< uint8_t , uint8_t > m_cursor;
		std::queue < std::shared_ptr < CEnemy > > m_spawnQueue;
		std::vector < std::shared_ptr < CEnemy > > ** m_fieldOfEnemies;
		std::shared_ptr < CTower > ** m_towerField;
		uint32_t m_ctr;
		uint32_t m_ctrMax;
		uint32_t m_spawnCtr;
		uint32_t m_positionCounter;
		CMatrix ** m_vissualMap;
		uint8_t m_ctrctr = 0;

};