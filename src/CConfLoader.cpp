#include <cassert>
#include "CConfLoader.h"

const std::string CConfLoader::CONF_FILE = "./examples/conf/conf";

CConfLoader::CConfLoader( )
	:	m_ifs ( CONF_FILE ),
		m_goodBit ( true )
{
	std::set < std::string > towerTypes;
	std::set < std::string > enemyTypes;
	for ( const std::string tower : CTowerMaker::TOWER_TYPES )
		towerTypes.emplace( tower );
	enemyTypes.emplace ( "Enemy" );
	if ( ! loadEnemyConf( enemyTypes ) )
		return;
	if ( ! loadTowerConf( towerTypes ) )
		return;
	if ( ! m_ifs.eof() )
		m_goodBit = false;
}

const std::map < 	std::string ,
	std::tuple < std::string , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t > > & CConfLoader::getEnemyConf ( void ) const
{
	return m_enemyConf;
}

const std::map < 	std::string , 
	std::tuple < std::string , uint8_t , uint8_t , uint8_t , uint8_t , uint8_t> > & CConfLoader::getTowerConf ( void ) const
{
	return m_towerConf;
}

CConfLoader::operator bool ( void )
{
	return m_goodBit;
}

bool CConfLoader::loadEnemyConf ( const std::set < std::string > & enemiesTypes  )
{
	std::string line;
	int numberOfEntities;
	char endChar;
	m_ifs >> line >>  numberOfEntities >> endChar;
	if (	! m_ifs || line != "<Enemy" ||
				numberOfEntities < 0 || numberOfEntities > 50 
				|| endChar != '>' )
	{
		m_goodBit = false;
		return false;
	}
	std::string name, type;
	int money, hp, dmg, attackSpeed, attackRange , iq , movementSpeed;
	for ( int i = 0 ; i < numberOfEntities ; i ++ )
	{
		m_ifs >> name >> money >> type >> hp >> dmg >> attackSpeed >> attackRange >> iq  >> movementSpeed;

		if 	(	! m_ifs ||
					money < 0 || money > UINT8_MAX ||
					hp <= 0 || hp > UINT8_MAX ||
					dmg <= 0 || dmg > UINT8_MAX ||
					attackSpeed <= 0 || attackSpeed > UINT8_MAX ||
					attackRange < 0 || attackRange > UINT8_MAX || 
					iq < 0 || iq > 2 ||
					movementSpeed <= 0 || movementSpeed > 60 ||
					( m_enemyConf.find( name ) != m_enemyConf.end() && m_enemyConf.size() != 0 ) ||
					enemiesTypes.find ( type ) == enemiesTypes.end() )
		{
			m_goodBit = false;
			return false;
		}
		m_enemyConf.emplace ( name ,
			std::make_tuple 
			( type , money , hp , dmg , attackSpeed , attackRange , iq  , movementSpeed ) );
	}
	return true;
}

bool CConfLoader::loadTowerConf ( const std::set < std::string > & towerTypes )
{
	std::string line;
	int numberOfEntities;
	char endChar;
	m_ifs >> line >> numberOfEntities >> endChar;
	if (	! m_ifs || line != "<Tower" ||
				numberOfEntities < 0 || numberOfEntities > 9 ||
				endChar != '>' )
	{
		m_goodBit = false;
		return false;
	}
	std::string name, type;
	int money, hp, dmg, attackSpeed, attackRange;
	for ( int i = 0 ; i < numberOfEntities ; i ++ )
	{
		m_ifs >> name >> money >> type >> hp >> dmg >> attackSpeed >> attackRange;
		if 	(	! m_ifs ||
					money < 0 || money > UINT8_MAX ||
					hp <= 0 || hp > UINT8_MAX ||
					dmg <= 0 || dmg > UINT8_MAX ||
					attackSpeed <= 0 || attackSpeed > UINT8_MAX ||
					attackRange <= 0 || attackRange > UINT8_MAX || 
					( m_towerConf.find( name ) != m_towerConf.end() && m_towerConf.size() != 0 ) ||
					towerTypes.find ( type ) == towerTypes.end() )
		{
			m_goodBit = false;
			return false;
		}
		m_towerConf.emplace ( name ,
													std::make_tuple ( type , money , hp , dmg , attackSpeed , attackRange ) );
	}
	return true;
}