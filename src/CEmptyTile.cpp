#include "CEmptyTile.h"

CEmptyTile::CEmptyTile( void )
	: CTile ()
{
	m_passeble = true;
	m_isTerrian = false;
	m_tileRepresentation.addRow( "     " )
											.addRow( "     " )
											.addRow( " ... " );
}

const char CEmptyTile::saveChar ( void ) const
{
	return '.';
}

bool CEmptyTile::canBeBuild ( bool canBePlacedOnTerrain ) const
{
	return true;
}

