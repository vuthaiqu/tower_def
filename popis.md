Jednoduchý tower defence kde se nepřátleské jednotky snaží dostat k cíly. Pokud se tak stane, hráčovi se ubere život a pokud nebude mít žádný, hra skončí prohrou. <br>
Naopak pokud se hráčovi podaří ubránit stupnující se hordě nepřetelských jednotek vyhraje hru.

Jednotlivý nepřátelé se liší rychlostí, chytrostí najít cestu k cíli, počtem životů, tyto údaje se dají změnit v conf souboru.
Na jednom políčku se mohou pohybovat více nepřátel najednou.

Hráč se bude moci pohybovat po políčkách "select" cursorem a stavět budovi za body, kteŕe dostane na začátku hry nebo za zabití nepřátel.

Budovy se liší typem.
Nevšechny budovy jsou však útočné. Některé budou schopny zvyšovat rychlost útoků a poškození ostatních věží a opravovat ostatní věže.
Nevšechny věže budou moci býtí postaveny na terénu. Budovy postavené na terénu nemůžou býti zautočeny od nepřátel.

Hráč bude moci načíst různé nepřátelské jednotky a věže ze souborů.

Také bude moci uložit/načíst hru v jakékoliv části.

Hrací pole bude vykresleno buňkami, které není jen jednocharakterové.

Použitý polymorfismus : věže a různé typy věží, algorythmy pohybu, obrazovky, políčka

Ovládání: Šipky a enter pro potvrzení
0-9 pro budování budov podle menu
Ukládní hry: Backspace
Ukončení hry: Q
Mazání věží: D

Struktura save souboru:
\<Money AMOUNT_OF_MONEY \>
\<HP AMOUNT_OF_HP \>
\<Map HEIGHT WIDTH \>
ASCII MAP LINE
.
.
.
ASCII MAP LINE
\<Towers NUMBER_OF_TURETS \>
YCORD XCORD NAME HP CHARGE DMG_UP ATUP_BUFF_TIME ATTACK_SPEEDUP ASUP_BUFF_TIME
.
.
.
YCORD XCORD NAME HP CHARGE DMG_UP ATUP_BUFF_TIME ATTACK_SPEEDUP ASUP_BUFF_TIME
\<Enemy_List NUMBER_OF_ENEMIES \>
YCORD XCORD NAME HP CHARGE ENERGY SLOW SLOW_DURATION
.
.
.
YCORD XCORD NAME HP CHARGE ENERGY SLOW SLOW_DURATION
\<Enemy_Queue NUMBER_QUEUED_ENEMIES \>
NAME
.
.
.
NAME


Struktura conf souboru
\<Enemy NUMBER_OF_TYPES \>
NAME LOOT TYPE HP DMG ATTACK_SPEED ATTACK_RANGE MOVEMENT_INTELIGENCE MOVEMENT_SPEED
.
.
.
NAME LOOT TYPE HP DMG ATTACK_SPEED ATTACK_RANGE MOVEMENT_INTELIGENCE MOVEMENT_SPEED
\<Tower NUMBER_OF_TYPES \>
NAME COST TYPE HP DMG ATTACK_SPEED ATTACK_RANGE
.
.
.
NAME COST TYPE HP DMG ATTACK_SPEED ATTACK_RANGE