CXX=g++
LD=g++
CXXFLAGS=-std=c++17 -Wall -pedantic -g
LIBS=-lncurses

BUILD_DIR := ./build/
SRC_DIRS := ./src/
TARGET_EXEC := game
TARGET_EXEC_DIR := ./

# Vsechny cpp soubory
SRCS := $(wildcard $(SRC_DIRS)*.cpp)

#vytvareni object filu
OBJS := $(SRCS:$(SRC_DIRS)%.cpp=$(BUILD_DIR)%.o)

#vytvareni dependecies filu
DEPS := $(OBJS:.o=.d)

# Flagy na vyvareni dependecies
CPPFLAGS := -MMD -MP

# Finalni kompulcae.
$(TARGET_EXEC_DIR)$(TARGET_EXEC): $(OBJS)
	$(CXX) $(OBJS) -o $@ $(LIBS)

# Vytvareni object filu a dependecies filu
$(BUILD_DIR)%.o: $(SRC_DIRS)%.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

all : doc compile run

compile : $(TARGET_EXEC_DIR)$(TARGET_EXEC)

run : $(TARGET_EXEC_DIR)$(TARGET_EXEC)
	$(TARGET_EXEC_DIR)$(TARGET_EXEC)

clean:
	rm -r $(BUILD_DIR)
	rm $($(TARGET_EXEC_DIR)$(TARGET_EXEC))

doc : Doxyfile $(wildcard $(SRC_DIRS)*.h)
	rm -r ./doc/
	doxygen Doxyfile

#inkluze dependecies filu
-include $(DEPS)